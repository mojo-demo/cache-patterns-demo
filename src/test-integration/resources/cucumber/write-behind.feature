@READ_THROUGH_WRITE_BEHIND_CACHE
Feature: Read Through Write Behind Cache (Hazelcast Caching)

  Scenario: Uploading Season Data first and verify it is persisted (write-behind cache)
    Given the system is initialized with no data
    When I upload season data from 'sample-data/epl-2020.json' to 'api/v1/football-results/read-through-write-behind'
    Then the system returns a created status
    And the response includes the number of rows inserted

  Scenario: Get all matches (write-behind cache)
    When I request all matches using the 'api/v1/football-results/read-through-write-behind' endpoint
    Then the system returns a success status
    And the response should contain a list of matches
    And the number of matches in the response should be the same as the number uploaded

  Scenario: Getting Matches by Season (write-behind cache)
    When I request matches for season '20/21' using the 'api/v1/football-results/read-through-write-behind/season/' endpoint
    Then the system returns a success status
    And the response should only contain season '20/21'
    And the number of matches in the response should be correct for season '20/21'

  Scenario: Getting a Single Match (write-behind cache)
    When I request a match by teams using the 'api/v1/football-results/read-through-write-behind/season/20%2F21/home_team/Fulham/away_team/Crystal+Palace' endpoint
    Then the system returns a success status
    And the match returned should be 'Fulham' V 'Crystal Palace' in '20/21'

  Scenario: Verifying data was persisted (write-behind cache)
    Given I wait for 15 seconds
    Then the number of records saved in the database is equal to the number uploaded

  Scenario: Finally clear persisted data
    Then clear all persisted data