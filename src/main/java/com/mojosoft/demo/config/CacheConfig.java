package com.mojosoft.demo.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionConfig;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MapStoreConfig;
import com.hazelcast.config.MaxSizePolicy;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import com.mojosoft.demo.cache.FootballResultsMapStore;
import com.mojosoft.demo.dto.FootballResultDTO;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * This class is used to set up the Spring cache configuration.
 */
@Configuration
@EnableCaching
@AllArgsConstructor
public class CacheConfig {

  private static final int WRITE_DELAY_SECONDS = 5;
  private static final int WRITE_BATCH_SIZE = 50;
  private static final int TTL_SECONDS = 3600; // TTL: 1 hour
  private static final int MAX_SIZE = 3000; // Max cache size
  public static final String FRSLT_READ_THROUGH_WRITE_BEHIND_CACHE =
      "footballReadThroughWriteBehindCache";
  public static final String FRSLT_SPRING_READWRITE_THROUGH_CACHE =
      "footballResultsCache";
  private final FootballResultsMapStore mapStore;

  /**
   * Creates the Hazelcast configuration bean.
   * Sets up the named Hazelcast map configurations used for caching.
   *
   * @return Configured Hazelcast instance
   */
  @Bean
  public Config hazelCastConfig() {
    Config config = new Config();
    config.addMapConfig(createMapConfig(FRSLT_SPRING_READWRITE_THROUGH_CACHE));
    config.addMapConfig(
        createMapStoreConfig(FRSLT_READ_THROUGH_WRITE_BEHIND_CACHE,
            mapStore,
            true));
    return config;
  }

  /**
   * Creates a basic MapConfig for Hazelcast caching.
   * Configures cache properties like TTL and eviction policy.
   *
   * @param mapName Name of the Hazelcast map
   * @return A configured MapConfig instance
   */
  private MapConfig createMapConfig(String mapName) {
    return new MapConfig(mapName)
        .setTimeToLiveSeconds(TTL_SECONDS)
        .setEvictionConfig(new EvictionConfig()
            .setMaxSizePolicy(MaxSizePolicy.FREE_HEAP_SIZE)
            .setSize(MAX_SIZE)
            .setEvictionPolicy(EvictionPolicy.LRU));
  }

  /**
   * Creates a MapConfig for Hazelcast caching with MapStore configuration.
   * Configures cache store behavior including write-behind properties.
   *
   * @param mapName Name of the map
   * @param mapStore The MapStore implementation
   * @param writeBehindEnabled Flag to enable write-behind caching
   * @return A configured MapConfig instance
   */
  private MapConfig createMapStoreConfig(
      String mapName,
      FootballResultsMapStore mapStore,
      boolean writeBehindEnabled) {

    MapConfig mapConfig = createMapConfig(mapName);
    MapStoreConfig mapStoreConfig = new MapStoreConfig()
        .setEnabled(true)
        //.setInitialLoadMode(MapStoreConfig.InitialLoadMode.EAGER)
        .setWriteCoalescing(true)
        .setImplementation(mapStore);

    if (writeBehindEnabled) {
      mapStoreConfig.setWriteDelaySeconds(WRITE_DELAY_SECONDS)
          .setWriteBatchSize(WRITE_BATCH_SIZE);
    }

    return mapConfig.setMapStoreConfig(mapStoreConfig);
  }

  /**
   * Defines a bean for the Hazelcast map used for read-through and write-behind caching.
   *
   * @param hazelcastInstance The Hazelcast instance
   * @return Hazelcast IMap configured for read-through and write-behind caching
   */
  @Bean(name = FRSLT_READ_THROUGH_WRITE_BEHIND_CACHE)
  public IMap<String, FootballResultDTO> footballReadThroughWriteBehindCache(
      @Lazy HazelcastInstance hazelcastInstance) {
    return hazelcastInstance.getMap(FRSLT_READ_THROUGH_WRITE_BEHIND_CACHE);
  }
}
