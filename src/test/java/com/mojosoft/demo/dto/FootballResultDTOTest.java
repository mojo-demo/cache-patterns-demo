package com.mojosoft.demo.dto;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import java.time.LocalDateTime;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class FootballResultDTOTest {

  private static Validator validator;

  @BeforeAll
  static void setup() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  @DisplayName("Given valid data, when validated, then no constraint violations are found")
  void whenValidDataThenNoConstraintViolations() {
    LocalDateTime matchDateTime = LocalDateTime.of(2021, 8, 1, 12, 0);
    FootballResultDTO dto = FootballResultDTO.of("Stadium", "HomeTeam", "AwayTeam", 2, 1, matchDateTime);

    Set<ConstraintViolation<FootballResultDTO>> violations = validator.validate(dto);
    assertThat(violations).as("Valid data should not result in any constraint violations").isEmpty();
  }

  @Test
  @DisplayName("Given invalid data, when validated, then appropriate constraint violations are found")
  void whenInvalidDataThenConstraintViolations() {
    LocalDateTime matchDateTime = LocalDateTime.of(2021, 8, 1, 12, 0);
    FootballResultDTO dto = FootballResultDTO.of("", "", "", -1, -1, matchDateTime);

    Set<ConstraintViolation<FootballResultDTO>> violations = validator.validate(dto);
    assertThat(violations).as("Invalid data should result in constraint violations").isNotEmpty();
    assertThat(violations).as("Expecting 5 violations for location, homeTeam, awayTeam, homeScore, and awayScore").hasSize(5);
  }

  @Test
  @DisplayName("Test factory method")
  void testFactoryMethod() {
    LocalDateTime matchDateTime = LocalDateTime.of(2021, 8, 1, 0, 0);
    FootballResultDTO dto = FootballResultDTO.of("Stadium", "HomeTeam", "AwayTeam", 2, 1, matchDateTime);

    assertThat(dto.season())
        .as("Check if the season is correctly calculated")
        .isEqualTo("21/22");
    assertThat(dto.location())
        .as("Check if the location matches")
        .isEqualTo("Stadium");
    assertThat(dto.homeTeam())
        .as("Check if the home team name matches")
        .isEqualTo("HomeTeam");
    assertThat(dto.awayTeam())
        .as("Check if the away team name matches")
        .isEqualTo("AwayTeam");
    assertThat(dto.homeScore())
        .as("Check if the home team score matches")
        .isEqualTo(2);
    assertThat(dto.awayScore())
        .as("Check if the away team score matches")
        .isEqualTo(1);
    assertThat(dto.matchDateTime())
        .as("Check if the match date and time match")
        .isEqualTo(matchDateTime);
  }
}