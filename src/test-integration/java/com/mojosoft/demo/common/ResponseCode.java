package com.mojosoft.demo.common;

import static org.assertj.core.api.Assertions.assertThat;

import io.cucumber.java.en.Then;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;

/**
 * This class contains the step definitions for response codes.
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class ResponseCode {

  @Autowired
  private ResponseMapper responseMapper;

  /**
   * This method checks the HTTP status code is ok.
   */
  @Then("the system returns a success status")
  public void httpStatusOk() {
    HttpStatusCode statusCode = responseMapper.getResponseEntity().getStatusCode();
    assertThat(statusCode)
        .as("The status code should be %s, but it is %s", HttpStatus.OK, statusCode)
        .isEqualTo(HttpStatus.OK);
  }

  /**
   * This method checks the HTTP status code is created.
   */
  @Then("the system returns a created status")
  public void httpStatusCreatedCheck() {
    HttpStatusCode statusCode = responseMapper.getResponseEntity().getStatusCode();
    assertThat(statusCode)
        .as("The status code should be %s, but it is %s", HttpStatus.CREATED, statusCode)
        .isEqualTo(HttpStatus.CREATED);
  }
}
