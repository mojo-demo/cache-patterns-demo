package com.mojosoft.demo.retrieve;

import static org.assertj.core.api.Assertions.assertThat;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import com.hazelcast.map.LocalMapStats;
import com.mojosoft.demo.common.ResponseMapper;
import com.mojosoft.demo.dto.FootballResultDTO;
import com.mojosoft.demo.repository.FootballResultRepository;
import com.mojosoft.demo.util.SeasonHelper;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

/**
 * This class contains the step definitions for retrieving results.
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Slf4j
public class GetResults {

  @Autowired
  HazelcastInstance hazelcastInstance;

  @Autowired
  private ResponseMapper responseMapper;

  @Autowired
  private FootballResultRepository footballResultRepository;

  @Autowired
  private TestRestTemplate restTemplate;

  /**
   * This method requests all matches using the specified endpoint.
   *
   * @param urlPath the URL path
   */
  @When("I request all matches using the {string} endpoint")
  public void allMatchesResultsEndpoint(String urlPath) {
    String baseUrl = restTemplate.getRootUri();
    String url = baseUrl + urlPath;

    ResponseEntity<List<FootballResultDTO>> response = restTemplate.exchange(
        url,
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<>() {}
    );

    assertThat(response).isNotNull();
    responseMapper.setResponseEntity(response);
  }

  /**
   * mark the current cache hit count.
   */
  @Given("the {string} cache hit count is noted")
  public void markCurrentCacheHitCount(String cacheName) {
    IMap<Object, Object> map = hazelcastInstance.getMap(cacheName);
    LocalMapStats mapStats = map.getLocalMapStats();
    responseMapper.setCacheHitCount(mapStats.getHits());
  }

  /**
   * Check that the cache hit count has increased by the expected amount.
   *
   * @param expectedIncrement the expected increment
   */
  @Then("the cache hit count should have increased by {int} since the last note")
  public void checkCacheHitCountIncrement(int expectedIncrement) {
    long expectedHitCount = responseMapper.getCacheHitCount() + expectedIncrement;

    IMap<Object, Object> map = hazelcastInstance.getMap("footballResultsCache");
    LocalMapStats mapStats = map.getLocalMapStats();
    long actualHitCount = mapStats.getHits();
    assertThat(actualHitCount)
        .as("The cache hit count is %s but should be %s", actualHitCount, expectedHitCount)
        .isEqualTo(expectedHitCount);
  }

  /**
   * Check that the response contains a list of matches.
   */
  @SuppressWarnings("all")
  @And("the response should contain a list of matches")
  public void containListOfMatches() {
    List<FootballResultDTO> response = (List<FootballResultDTO>) responseMapper.getResponseEntity().getBody();
    assertThat(response).isNotNull();
    assertThat(response).isNotEmpty();
  }

  /**
   * Check that the response contains a list of matches.
   */
  @SuppressWarnings("all")
  @And("the number of matches in the response should be the same as the number uploaded")
  public void checkNumberOfMatchesUploaded() {
    int expectedCount = responseMapper.getUploadedFootballResults().size();

    int actualCount = ((List<FootballResultDTO>)
        responseMapper.getResponseEntity().getBody()).size();

    assertThat(actualCount)
        .as("The number of matches in the response should be the same as the number uploaded")
        .isEqualTo(expectedCount);
  }

  /**
   * Request matches for the specified season.
   *
   * @param season the season
   * @param endpoint the endpoint
   */
  @When("I request matches for season {string} using the {string} endpoint")
  public void requestMatches(String season, String endpoint) {
    String baseUrl = restTemplate.getRootUri();
    String seasonEncoded = URLEncoder.encode(season, StandardCharsets.UTF_8);
    String url = baseUrl + endpoint + seasonEncoded;

    ResponseEntity<List<FootballResultDTO>> response = restTemplate.exchange(
        url,
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<>() {}
    );

    assertThat(response).isNotNull();
    responseMapper.setResponseEntity(response);
  }

  /**
   * Check that the response contains a list of matches in the season.
   *
   * @param season the season
   */
  @And("the response should only contain season {string}")
  @SuppressWarnings("all")
  public void theResponseShouldOnlyContainTheSpecifiedSeason(String season) {
    List<FootballResultDTO> response = (List<FootballResultDTO>)
        responseMapper.getResponseEntity().getBody();
    for (FootballResultDTO footballResultDTO : response) {
      assertThat(footballResultDTO.season())
          .as("The season should be the same as the one specified in the request")
          .isEqualTo(season);
    }
  }

  /**
   * Check that the response contains a list of matches in the season.
   *
   * @param season the season
   */
  @SuppressWarnings("all")
  @And("the number of matches in the response should be correct for season {string}")
  public void theNumberOfMatchesInTheResponseShouldBeCorrectForTheSpecifiedSeason(String season) {
    long expectedCount = responseMapper.getUploadedFootballResults().stream().filter(
        footballResultDTO -> extractSeason(footballResultDTO.getDateUtc()).equals(season)).count();
    long actualCount = ((List<FootballResultDTO>)
        responseMapper.getResponseEntity().getBody()).size();

    assertThat(actualCount)
        .as("The number of matches in the response should be the same as the number uploaded")
        .isEqualTo(expectedCount);
  }

  /**
   * Request a match by teams.
   *
   * @param endpoint the endpoint
   */
  @When("I request a match by teams using the {string} endpoint")
  public void requestByTeamsPlayed(String endpoint) {
    String baseUrl = restTemplate.getRootUri();
    String url = baseUrl + endpoint;

    ResponseEntity<FootballResultDTO> response = restTemplate.exchange(
        url,
        HttpMethod.GET,
        null,
        FootballResultDTO.class
    );

    assertThat(response).isNotNull();
    responseMapper.setResponseEntity(response);
  }

  /**
   * Check that the response contains the single match.
   */
  @And("the match returned should be {string} V {string} in {string}")
  public void assertResponseOfRequestByTeamsPlayed(
      String homeTeam,
      String awayTeam,
      String season) {
    FootballResultDTO actualResult =
        (FootballResultDTO) responseMapper.getResponseEntity().getBody();
    assertThat(actualResult).isNotNull();
    assertThat(actualResult.homeTeam()).isEqualTo(homeTeam);
    assertThat(actualResult.awayTeam()).isEqualTo(awayTeam);
    assertThat(actualResult.season()).isEqualTo(season);
  }


  private static String extractSeason(String dateTimeStr) {

    LocalDateTime localDateTime;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss'Z'");
    try {
      localDateTime = LocalDateTime.parse(dateTimeStr, formatter);
    } catch (Exception e) {
      localDateTime = LocalDateTime.parse(dateTimeStr, DateTimeFormatter.ISO_DATE_TIME);
    }
    return SeasonHelper.calculateSeason(localDateTime);
  }

  /**
   * Check the number of records saved in the database is equal to the number uploaded.
   */
  @Then("the number of records saved in the database is equal to the number uploaded")
  public void theNumberOfRecordsSavedInTheDatabaseIsEqualToTheNumberUploaded() {
    long actualCount = responseMapper.getUploadedFootballResults().size();
    long expectedCount = footballResultRepository.count();

    assertThat(actualCount)
        .as("The number of records in db should be %s but is %s",
            expectedCount, actualCount)
        .isEqualTo(expectedCount);
  }
}