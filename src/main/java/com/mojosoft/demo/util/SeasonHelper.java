package com.mojosoft.demo.util;

import java.time.LocalDateTime;

/**
 * Helper class for calculating season.
 */
public class SeasonHelper {
  /**
   * This method calculates the season based on the date.
   *
   * @param date the date
   * @return the season
   */
  public static String calculateSeason(LocalDateTime date) {
    int year = date.getYear();
    String seasonStart;
    String seasonEnd;

    if (date.getMonthValue() >= 8) { // August or later
      seasonStart = String.format("%02d", year % 100);
      seasonEnd = String.format("%02d", (year + 1) % 100);
    } else { // January to July
      seasonStart = String.format("%02d", (year - 1) % 100);
      seasonEnd = String.format("%02d", year % 100);
    }

    return seasonStart + "/" + seasonEnd;
  }
}
