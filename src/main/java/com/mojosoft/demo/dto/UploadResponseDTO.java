package com.mojosoft.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO for upload response.
 */
@SuppressWarnings("checkstyle:AbbreviationAsWordInName")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UploadResponseDTO {

  private int recordsProcessed;
  private String message;
  private ErrorDetails error;

  public UploadResponseDTO(int recordsProcessed, String message) {
    this.recordsProcessed = recordsProcessed;
    this.message = message;
  }

  /**
   * This class sets the error details.
   */
  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class ErrorDetails {
    private String message;
    private String type;
  }
}

