package com.mojosoft.demo.common;

import com.mojosoft.demo.dto.FootballResultJsonDTO;
import java.util.List;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
  Cucumber does not support polymorphism, this class is created just to hold the shared states.
 */
@Component
@Data
public class ResponseMapper {
  private long cacheHitCount;
  private ResponseEntity<?> responseEntity;
  private List<FootballResultJsonDTO> uploadedFootballResults;
}
