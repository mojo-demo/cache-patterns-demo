package com.mojosoft.demo.dto;

import com.mojosoft.demo.util.SeasonHelper;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * Football result data transfer object.
 */
@SuppressWarnings("checkstyle:AbbreviationAsWordInName")
public record FootballResultDTO(
    String season,
    @NotNull @Size(min = 3, max = 50) String location,
    @NotNull @Size(min = 3, max = 50) String homeTeam,
    @NotNull @Size(min = 3, max = 50) String awayTeam,
    @Min(value = 0) int homeScore,
    @Min(value = 0) int awayScore,
    @NotNull LocalDateTime matchDateTime
) {
  /**
   * Creates a new instance of {@link FootballResultDTO}.
   *
   * @param location  the location of the match
   * @param homeTeam  the home team
   * @param awayTeam  the away team
   * @param homeScore the home team score
   * @param awayScore the away team score
   * @param matchDateTime the datetime of the match
   * @return a new instance of {@link FootballResultDTO}
   */
  public static FootballResultDTO of(
      @Size(min = 3, max = 50) String location,
      @Size(min = 3, max = 50) String homeTeam,
      @Size(min = 3, max = 50) String awayTeam,
      @Min(value = 0) int homeScore,
      @Min(value = 0) int awayScore,
      @NotNull LocalDateTime matchDateTime) {

    String season = SeasonHelper.calculateSeason(matchDateTime);
    return new FootballResultDTO(
        season,
        location,
        homeTeam,
        awayTeam,
        homeScore,
        awayScore,
        matchDateTime);
  }

}
