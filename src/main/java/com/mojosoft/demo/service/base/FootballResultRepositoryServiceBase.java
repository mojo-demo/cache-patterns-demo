package com.mojosoft.demo.service.base;

import com.mojosoft.demo.dto.FootballResultDTO;
import com.mojosoft.demo.dto.FootballResultJsonDTO;
import com.mojosoft.demo.dto.UploadResponseDTO;
import com.mojosoft.demo.entity.FootballResult;
import com.mojosoft.demo.mapper.MapperHelper;
import com.mojosoft.demo.repository.FootballResultRepository;
import com.mojosoft.demo.repository.FootballResultSaveOrUpdateHelper;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class for managing football results with no cache.
 */
@Slf4j
@Transactional
public class FootballResultRepositoryServiceBase implements FootballResultServiceI {

  private final FootballResultRepositoryServiceWriteThrough writeThroughService;
  private final FootballResultRepository footballResultRepository;
  private final MapperHelper mapper;
  private final FootballResultSaveOrUpdateHelper footballResultSaveOrUpdateHelper;

  /**
   * Instantiates a new Football result service.
   *
   * @param footballResultRepository the football result repository
   * @param mapper the mapper
   */
  public FootballResultRepositoryServiceBase(
      FootballResultSaveOrUpdateHelper footballResultSaveOrUpdateHelper,
      FootballResultRepository footballResultRepository,
      MapperHelper mapper) {
    this(footballResultSaveOrUpdateHelper, footballResultRepository, mapper, null);
  }

  /**
   * Instantiates a new Football result service.
   *
   * @param writeThroughService the write-through service
   * @param footballResultRepository the football result repository
   * @param mapper the mapper
   */
  public FootballResultRepositoryServiceBase(
      FootballResultSaveOrUpdateHelper footballResultSaveOrUpdateHelper,
      FootballResultRepository footballResultRepository,
      MapperHelper mapper,
      FootballResultRepositoryServiceWriteThrough writeThroughService) {
    this.mapper = mapper;
    this.footballResultRepository = footballResultRepository;
    this.writeThroughService = writeThroughService;
    this.footballResultSaveOrUpdateHelper = footballResultSaveOrUpdateHelper;
  }

  @Override
  public UploadResponseDTO save(List<FootballResultJsonDTO> results)  {
    int recordsSaved;
    List<FootballResultDTO> footballResultsDto = results.stream()
        .map(mapper::footballMatchJsonToDto)
        .collect(Collectors.toList());

    if (writeThroughService == null) {
      recordsSaved = footballResultSaveOrUpdateHelper.saveAll(footballResultsDto).size();
    } else {
      recordsSaved = writeThroughService.save(footballResultsDto).size();
    }

    String message = String.format("Successfully uploaded %d football results.", recordsSaved);
    return new UploadResponseDTO(recordsSaved, message, null);
  }

  @Override
  public List<FootballResultDTO> getAll() {
    return footballResultRepository.findAll().stream()
        .map(mapper::footballResultToDto)
        .collect(Collectors.toList());
  }

  @Override
  public List<FootballResultDTO> getBySeason(String season) {
    return footballResultRepository.findAllBySeason(season).stream()
        .map(mapper::footballResultToDto)
        .collect(Collectors.toList());
  }

  @Override
  public Optional<FootballResultDTO> getByMatch(String season, String homeTeam, String awayTeam) {
    Optional<FootballResult> result = footballResultRepository
        .findBySeasonAndHomeTeamAndAwayTeam(season, homeTeam, awayTeam);
    return result.isPresent() ? result.map(mapper::footballResultToDto) : Optional.empty();
  }
}
