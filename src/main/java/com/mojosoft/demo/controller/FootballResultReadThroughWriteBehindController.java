package com.mojosoft.demo.controller;

import com.mojosoft.demo.controller.base.FootballResultControllerBase;
import com.mojosoft.demo.service.FootballResultReadThroughWriteBehindService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class for testing football results with read through write behind cache.
 */
@RestController
@Tag(name = "Football Results (Cache Read Through Write Behind)")
@RequestMapping("/api/v1/football-results/read-through-write-behind")
@Slf4j
public class FootballResultReadThroughWriteBehindController extends FootballResultControllerBase {

  public FootballResultReadThroughWriteBehindController(
      FootballResultReadThroughWriteBehindService resultService) {
    super(resultService);
  }
}