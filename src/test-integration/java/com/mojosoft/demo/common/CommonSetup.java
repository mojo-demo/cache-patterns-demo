package com.mojosoft.demo.common;

import io.cucumber.java.Before;
import io.cucumber.java.en.Then;

/**
 * This class contains common setup steps.
 */
public class CommonSetup {

  @Before
  public void before() {
  }

  @Then("I wait for {int} seconds")
  public void waitForSeconds(int seconds) throws InterruptedException {
    Thread.sleep(seconds * 1000L);
  }
}
