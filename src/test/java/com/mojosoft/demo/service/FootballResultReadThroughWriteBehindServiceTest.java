package com.mojosoft.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import com.hazelcast.map.IMap;
import com.hazelcast.query.Predicate;
import com.mojosoft.demo.dto.FootballResultDTO;
import com.mojosoft.demo.dto.FootballResultJsonDTO;
import com.mojosoft.demo.dto.UploadResponseDTO;
import com.mojosoft.demo.mapper.MapperHelper;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Unit tests for FootballResultReadThroughWriteBehindService.
 */
@ExtendWith(MockitoExtension.class)
class FootballResultReadThroughWriteBehindServiceTest {

  @Mock
  private IMap<String, FootballResultDTO> resultsMap;

  @Mock
  private MapperHelper mapper;

  @InjectMocks
  private FootballResultReadThroughWriteBehindService footballResultService;

  @Test
  @DisplayName("Save football results without cache")
  void testSaveResults() {
    List<FootballResultJsonDTO> resultJsonDTOS = List.of(new FootballResultJsonDTO());
    FootballResultDTO footballResultDTO = getSampleFootballResultDTO();
    when(mapper.footballMatchJsonToDto(any(FootballResultJsonDTO.class))).thenAnswer(invocation -> footballResultDTO);
    doNothing().when(resultsMap).putAll(any());

    UploadResponseDTO response = footballResultService.save(resultJsonDTOS);

    assertThat(response).isNotNull();
    assertThat(response.getRecordsProcessed()).isEqualTo(1);
    assertThat(response.getMessage())
        .isEqualTo(String.format("Successfully uploaded %d football results.", 1));

    verify(resultsMap).putAll(any());
  }

  @Test
  @DisplayName("Retrieve all football results")
  void testGetAll() {
    when(resultsMap.values()).thenReturn(
        List.of(getSampleFootballResultDTO(), getSampleFootballResultDTO()));

    List<FootballResultDTO> actualDTOS = footballResultService.getAll();

    assertThat(actualDTOS).isNotNull();
    assertThat(actualDTOS.size()).isEqualTo(2);

    verify(resultsMap).values();
  }

  @SuppressWarnings("unchecked")
  @Test
  @DisplayName("Retrieve football results by season")
  void testGetBySeason() {
    when(resultsMap.values(any(Predicate.class))).thenReturn(
        List.of(getSampleFootballResultDTO(), getSampleFootballResultDTO()));

    List<FootballResultDTO> actualDTOS = footballResultService.getBySeason("21/22");

    assertThat(actualDTOS).isNotNull();
    assertThat(actualDTOS.size()).isEqualTo(2);

    verify(resultsMap).values(any());
  }

  @SuppressWarnings("unchecked")
  @Test
  @DisplayName("Retrieve football result by season, home team, and away team")
  void testGetByMatch() {
    when(resultsMap.values(any(Predicate.class))).thenReturn(
        List.of(getSampleFootballResultDTO()));

    Optional<FootballResultDTO> actualDTOS =
        footballResultService.getByMatch(
            "21/22",
            "HomeTeam1",
            "AwayTeam1"
        );

    assertThat(actualDTOS.isPresent()).isTrue();
    verify(resultsMap).values(any());
  }

  private FootballResultDTO getSampleFootballResultDTO() {
    return new FootballResultDTO("21/22", "Stadium1", "HomeTeam1", "AwayTeam1", 2, 1, LocalDateTime.now());
  }
}