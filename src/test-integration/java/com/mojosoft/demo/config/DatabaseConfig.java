package com.mojosoft.demo.config;

import javax.sql.DataSource;
import liquibase.integration.spring.SpringLiquibase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;

/**
 * This class is used to configure the database for integration tests.
 */
@Slf4j
@Configuration
public class DatabaseConfig {

  @Value("${docker.image.name:postgres:13.3-alpine}")
  private String dockerImageName;

  @Value("${db.port:5432}")
  private int dbPort;

  @Value("${db.name:testdb}")
  private String dbName;

  @Value("${db.username:postgres}")
  private String dbUsername;

  @Value("${db.password:password}")
  private String dbPassword;

  public static PostgreSQLContainer<?> postgres;

  /**
   * This method is used to configure the database for integration tests.
   *
   * @return the configured database
   */
  @Bean
  public PostgreSQLContainer<?> postgresContainer() {
    DockerImageName ecrPostgresImage = DockerImageName.parse(dockerImageName)
        .asCompatibleSubstituteFor("postgres");
    postgres = new PostgreSQLContainer<>(ecrPostgresImage)
        .withDatabaseName(dbName)
        .withUsername(dbUsername)
        .withPassword(dbPassword)
        .withExposedPorts(dbPort);
    postgres.start();
    return postgres;
  }

  /**
   * This method is used to configure the data source for integration tests.
   *
   * @param postgresContainer the configured database
   * @return the configured data source
   */
  @Bean
  @Primary
  @DependsOn("postgresContainer")
  public DataSource dataSource(PostgreSQLContainer<?> postgresContainer) {
    log.info("************************************************************");
    log.info("Postgres Port: {}", postgres.getFirstMappedPort());
    log.info("************************************************************");

    return DataSourceBuilder.create()
        .driverClassName(postgresContainer.getDriverClassName())
        .url(postgresContainer.getJdbcUrl())
        .username(postgresContainer.getUsername())
        .password(postgresContainer.getPassword())
        .build();
  }

  /**
   * This method is used to configure the liquibase for integration tests.
   *
   * @param dataSource the configured data source
   * @return the configured liquibase
   */
  @Bean
  public SpringLiquibase springLiquibase(DataSource dataSource) {
    SpringLiquibase liquibase = new SpringLiquibase();
    liquibase.setDataSource(dataSource);
    liquibase.setChangeLog("classpath:db/changelog/db.changelog-master.yaml");
    return liquibase;
  }
}
