package com.mojosoft.demo.service;

import com.mojosoft.demo.dto.FootballResultDTO;
import com.mojosoft.demo.dto.FootballResultJsonDTO;
import com.mojosoft.demo.dto.UploadResponseDTO;
import com.mojosoft.demo.mapper.MapperHelper;
import com.mojosoft.demo.repository.FootballResultRepository;
import com.mojosoft.demo.repository.FootballResultSaveOrUpdateHelper;
import com.mojosoft.demo.service.base.FootballResultRepositoryServiceBase;
import com.mojosoft.demo.service.base.FootballResultRepositoryServiceWriteThrough;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class for managing football results with no cache.
 */
@Service("footballResultSpringReadWriteThroughService")
@Slf4j
@Transactional
public class FootballResultSpringReadWriteThroughService
    extends FootballResultRepositoryServiceBase {

  public static final String FOOTBALL_RESULTS_CACHE = "footballResultsCache";

  public FootballResultSpringReadWriteThroughService(
      FootballResultSaveOrUpdateHelper footballResultSaveOrUpdateHelper,
      FootballResultRepositoryServiceWriteThrough writeThroughService,
      FootballResultRepository footballResultRepository,
      MapperHelper mapper) {
    super(footballResultSaveOrUpdateHelper, footballResultRepository, mapper, writeThroughService);
  }

  /**
   * This method is used to persist football results.
   * Note. Write-through caching is handled by underlying save method using CachePut.
   *
   * @param results the football results to upload
   * @return the upload response
   */
  @Override
  public UploadResponseDTO save(List<FootballResultJsonDTO> results)  {
    return super.save(results);
  }

  @Override
  @Cacheable(value = FOOTBALL_RESULTS_CACHE, key = "'all'")
  public List<FootballResultDTO> getAll() {
    return super.getAll();
  }

  @Override
  @Cacheable(value = FOOTBALL_RESULTS_CACHE, key = "#season")
  public List<FootballResultDTO> getBySeason(String season) {
    return super.getBySeason(season);
  }

  @Override
  @Cacheable(value = FOOTBALL_RESULTS_CACHE, key = "{#season, #homeTeam, #awayTeam}")
  public Optional<FootballResultDTO> getByMatch(String season, String homeTeam, String awayTeam) {
    return super.getByMatch(season, homeTeam, awayTeam);
  }
}
