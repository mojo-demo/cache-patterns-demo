@WRITE_THROUGH_CACHE
Feature: Write-Through Cache (Spring Boot caching)

  Scenario: Uploading Season Data first and verify it is persisted (write-through cache)
    Given the system is initialized with no data
    When I upload season data from 'sample-data/epl-2020.json' to 'api/v1/football-results/spring-readwrite-through'
    Then the system returns a created status
    And the response includes the number of rows inserted
    And all the uploaded results are stored
    And the 'footballResultsCache' is not empty

  Scenario: Get all matches (Should hit the cache as we are configured to use write-through)
    Given the 'footballResultsCache' cache hit count is noted
    When I request all matches using the 'api/v1/football-results/spring-readwrite-through' endpoint
    Then the system returns a success status
    And the response should contain a list of matches
    And the number of matches in the response should be the same as the number uploaded
    And the cache hit count should have increased by 1 since the last note

