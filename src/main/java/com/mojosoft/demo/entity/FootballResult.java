package com.mojosoft.demo.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * This is a simple entity class that represents a football result.
 */
@Entity
@Table(name = "football_result")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FootballResult {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @NotNull(message = "Season cannot be null")
  @Pattern(regexp = "\\d{2}/\\d{2}", message = "Season must be in the format YY/YY")
  private String season;
  private String location;
  private String homeTeam;
  private String awayTeam;
  private int homeScore;
  private int awayScore;
  @NotNull(message = "DateTime cannot be null")
  private LocalDateTime matchDateTime;
}
