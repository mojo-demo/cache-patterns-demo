import groovy.json.JsonSlurper

task startDatabase {
    doLast {
        println 'Starting db...'
        exec {
            commandLine 'docker-compose', 'up', '--no-deps', '--build', '--detach', 'db'
        }

        // Wait until the database is up and ready
        println 'Waiting for the database to be ready...'
        while (!isDatabaseReady()) {
            sleep(5000) // Wait for 5 seconds before checking again
        }

        println 'Database is ready!'
    }

    group 'Database'
    description 'Starts the db using Docker Compose'
}

Process executeDBCommand(List<String> command, int timeout) {
    def process = command.execute()
    process.waitForOrKill(timeout)
    process
}

boolean isDatabaseReady() {
    def result = executeDBCommand(['docker-compose', 'ps', '-q', 'db'], 10000)
    if (result.exitValue() == 0) {
        def containerId = result.text.trim()
        result = executeDBCommand(['docker', 'inspect', '--format', '{{json .State.Health.Status}}', containerId], 10000)
        if (result.exitValue() == 0) {
            def status = new JsonSlurper().parseText(result.text.trim())
            return status == 'healthy'
        }
    }
    return false
}

task listDatabase {
    doLast {
        println 'Listing db...'
        exec {
            commandLine 'docker-compose', 'top'
            ignoreExitValue = true
        }
    }
    group 'Database'
    description 'List the active services using Docker Compose'
}

task stopDatabase {
    doLast {
        println 'Stopping db...'
        exec {
            commandLine 'docker-compose', 'stop'
        }
    }
    group 'Database'
    description 'Stops the db'
}

task destroyDatabase {
    doLast {
        println 'Stopping and dropping db...'
        exec {
            commandLine 'docker-compose', 'down'
        }
    }
    group 'Database'
    description 'Stop and drop the db'
}

task database {
    description 'Runs postgress db and starts/stops using Docker Compose'
}

bootRun.dependsOn startDatabase

tasks.bootRun.doLast {
    stopDatabase.execute()
}