package com.mojosoft.demo.controller;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

import com.mojosoft.demo.service.FootballResultReadThroughWriteBehindService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(MockitoExtension.class)
class FootballResultReadThroughWriteBehindControllerTest {

  @Mock
  private FootballResultReadThroughWriteBehindService footballResultService;

  @InjectMocks
  protected FootballResultReadThroughWriteBehindController footballResultController;

  private FootballResultControllerTestHelper helper;

  @BeforeEach
  void setup() {
    MockMvc mockMvc = standaloneSetup(footballResultController).build();
    String baseUrl = "/api/v1/football-results/read-through-write-behind";
    helper = new FootballResultControllerTestHelper(baseUrl, footballResultService, footballResultController, mockMvc);
  }

  @Test
  @DisplayName("Test getAll Football Results (Read Through Write Behind)")
  void testGetAllFootballResults() throws Exception {
    helper.assertGetAllFootballResults();
  }

  @Test
  @DisplayName("Test getFootballResultsBySeason (Read Through Write Behind)")
  void testGetFootballResultsBySeason() throws Exception {
    helper.assertGetFootballResultsBySeason();
  }

  @Test
  @DisplayName("Test getFootballResultsByMatch (Read Through Write Behind)")
  void getFootballResultByMatch() throws Exception {
    helper.assertGetFootballResultByMatch();
  }

  @Test
  @DisplayName("Test uploading football results without cache (Read Through Write Behind)")
  void uploadFootballResultsNoCache() throws Exception {
    helper.assertUploadFootballResults();
  }
}
