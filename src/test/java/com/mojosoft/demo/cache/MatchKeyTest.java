package com.mojosoft.demo.cache;

import com.mojosoft.demo.dto.FootballResultDTO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class MatchKeyTest {

  @Test
  @DisplayName("Create key from season, home team, and away team")
  void createKeyFromSeasonHomeAway() {
    String season = "21/22";
    String homeTeam = "HomeTeam";
    String awayTeam = "AwayTeam";

    String key = MatchKey.createKey(season, homeTeam, awayTeam);

    assertThat(key).isEqualTo("21/22_HomeTeam_AwayTeam");
  }

  @Test
  @DisplayName("Create key from FootballResultDTO")
  void createKeyFromDTO() {
    FootballResultDTO dto = getSampleFootballResult();
    String key = MatchKey.createKey(dto);
    assertThat(key).isEqualTo("21/22_HomeTeam1_AwayTeam1");
  }

  @Test
  @DisplayName("Extract key components from valid key")
  void extractKeyComponentsValidKey() {
    String key = "21/22_HomeTeam_AwayTeam";
    MatchKey matchKey = MatchKey.extractKeyComponents(key);
    assertThat(matchKey).isEqualTo(new MatchKey("21/22", "HomeTeam", "AwayTeam"));
  }

  @Test
  @DisplayName("Throw exception for invalid key format")
  void extractKeyComponentsInvalidKey() {
    String key = "invalid_key";

    assertThatThrownBy(() -> MatchKey.extractKeyComponents(key))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Invalid key format");
  }

  private FootballResultDTO getSampleFootballResult() {
    return new FootballResultDTO("21/22", "Stadium1", "HomeTeam1", "AwayTeam1", 2, 1, LocalDateTime.now());
  }

}
