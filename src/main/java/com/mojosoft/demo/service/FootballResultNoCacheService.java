package com.mojosoft.demo.service;

import com.mojosoft.demo.mapper.MapperHelper;
import com.mojosoft.demo.repository.FootballResultRepository;
import com.mojosoft.demo.repository.FootballResultSaveOrUpdateHelper;
import com.mojosoft.demo.service.base.FootballResultRepositoryServiceBase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class for managing football results with no cache.
 */
@Service("footballResultNoCacheService")
@Slf4j
@Transactional
public class FootballResultNoCacheService extends FootballResultRepositoryServiceBase {

  public FootballResultNoCacheService(
      FootballResultSaveOrUpdateHelper footballResultSaveOrUpdateHelper,
      FootballResultRepository footballResultRepository,
      MapperHelper mapper) {
    super(footballResultSaveOrUpdateHelper, footballResultRepository, mapper);
  }
}