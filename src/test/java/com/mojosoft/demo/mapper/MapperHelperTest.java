package com.mojosoft.demo.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import com.mojosoft.demo.dto.FootballResultDTO;
import com.mojosoft.demo.dto.FootballResultJsonDTO;
import com.mojosoft.demo.entity.FootballResult;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.mojosoft.demo.util.SeasonHelper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

/**
 * Map Helper Test.
 */
class MapperHelperTest {

  private final MapperHelper mapperHelper = Mappers.getMapper(MapperHelper.class);

  @Test
  @DisplayName("Test mapping from entity to DTO")
  @SuppressWarnings("all")
  void testFootballResultToDto() {
    // Given
    FootballResult footballResult = new FootballResult();
    footballResult.setId(1L);
    footballResult.setHomeTeam("Team A");
    footballResult.setAwayTeam("Team B");
    footballResult.setLocation("Location");
    footballResult.setSeason("20/21");
    footballResult.setHomeScore(1);
    footballResult.setAwayScore(2);
    footballResult.setMatchDateTime(LocalDateTime.of(2020, 10, 1, 10, 0));


    // When
    FootballResultDTO resultDTO = mapperHelper.footballResultToDto(footballResult);

    // Then
    assertThat(resultDTO)
        .as("Check if mapped DTO matches expected fields of entity")
        .usingRecursiveComparison()
        .ignoringFields("id")
        .isEqualTo(footballResult);
  }


  @Test
  @DisplayName("Test mapping from DTO to entity")
  @SuppressWarnings("all")
  void testFootballResultToEntity() {

    FootballResultDTO footballResultDTO = FootballResultDTO.of(
        "Location",
        "Team A",
        "Team B",
        2,
        1,
        LocalDateTime.of(2020, 10, 1, 10, 0)
    );

    FootballResult result = mapperHelper.footballResultToEntity(footballResultDTO);

    assertThat(result)
        .as("Check if mapped DTO matches expected fields of entity")
        .usingRecursiveComparison()
        .ignoringFields("id")
        .isEqualTo(footballResultDTO);
  }

  @Test
  @DisplayName("Test mapping from JSON to entity")
  @SuppressWarnings("all")
  void testFootballResultJsonToEntity() {
    FootballResultJsonDTO jsonDto = FootballResultJsonDTO.builder()
        .matchNumber(1L)
        .dateUtc("2023-12-19T17:25:46.087902")
        .location("Stadium")
        .homeTeam("Team A")
        .awayTeam("Team B")
        .homeTeamScore(2)
        .awayTeamScore(1)
        .build();

    FootballResult result = mapperHelper.footballMatchJsonToEntity(jsonDto);

    assertThat(result).isNotNull();
    assertThat(result.getMatchDateTime()).isEqualTo(jsonDto.getDateUtc());
    assertThat(result.getHomeTeam()).isEqualTo(jsonDto.getHomeTeam());
    assertThat(result.getAwayTeam()).isEqualTo(jsonDto.getAwayTeam());
    assertThat(result.getLocation()).isEqualTo(jsonDto.getLocation());
    assertThat(result.getHomeScore()).isEqualTo(jsonDto.getHomeTeamScore());
    assertThat(result.getAwayScore()).isEqualTo(jsonDto.getAwayTeamScore());
  }

  @Test
  @DisplayName("Map String date to LocalDateTime")
  void testStringToLocalDateTime() {
    String date = "2023-12-19T17:25:46.087902";
    LocalDateTime expectedDateTime = LocalDateTime.parse(date, DateTimeFormatter.ISO_DATE_TIME);
    LocalDateTime resultDateTime = mapperHelper.stringToLocalDateTime(date);
    assertThat(resultDateTime).isEqualTo(expectedDateTime);
  }

  @Test
  @DisplayName("Map FootballResultJsonDTO to FootballResultDTO")
  void testFootballMatchJsonToDto() {
    FootballResultJsonDTO jsonDto = createSampleFootballResultJsonDTO();
    FootballResultDTO result = mapperHelper.footballMatchJsonToDto(jsonDto);

    assertThat(result).isNotNull();
    assertThat(result.matchDateTime()).isEqualTo(jsonDto.getDateUtc());
    assertThat(result.homeTeam()).isEqualTo(jsonDto.getHomeTeam());
    assertThat(result.awayTeam()).isEqualTo(jsonDto.getAwayTeam());
    assertThat(result.location()).isEqualTo(jsonDto.getLocation());
    assertThat(result.homeScore()).isEqualTo(jsonDto.getHomeTeamScore());
    assertThat(result.awayScore()).isEqualTo(jsonDto.getAwayTeamScore());
  }

  @Test
  @DisplayName("Check Equality Between FootballResultDTO and FootballResult")
  void testIsEqual() {

    LocalDateTime resultTime = (LocalDateTime.now().withYear(2021).withMonth(10));
    FootballResult footballResult = createSampleFootballResult(resultTime);
    FootballResultDTO footballResultDTO = createSampleFootballResultDTO(resultTime);

    boolean result = mapperHelper.isEqual(footballResultDTO, footballResult);

    assertThat(result).isTrue();
  }

  @Test
  @DisplayName("Should return true when both dto and entity are null")
  public void testReturnTrueWhenBothDtoAndEntityAreNull() {
    assertThat(mapperHelper.isEqual(null, null)).isTrue();
  }

  @Test
  @DisplayName("Should return false when dto is null")
  public void testReturnFalseWhenDtoIsNull() {
    FootballResult entity = new FootballResult(); // Assuming a default constructor
    assertThat(mapperHelper.isEqual(null, entity)).isFalse();
  }

  @Test
  @DisplayName("Should return false when entity is null")
  public void testReturnFalseWhenEntityIsNull() {
    FootballResultDTO footballResultDTO = createSampleFootballResultDTO(null);
    assertThat(mapperHelper.isEqual(footballResultDTO, null)).isFalse();
  }

  @Test
  @DisplayName("Update FootballResult Entity from FootballResultDTO")
  @SuppressWarnings("all")
  void testUpdateFootballResultEntityFromDto() {
    FootballResult footballResult = createSampleFootballResult(null);
    FootballResultDTO footballResultDTO = createSampleFootballResultDTO(null);

    mapperHelper.updateFootballResultEntityFromDto(footballResultDTO, footballResult);

    assertThat(footballResult)
        .as("Check if mapped json matches expected fields of dto")
        .usingRecursiveComparison()
        .ignoringFields("id")
        .isEqualTo(footballResultDTO);
  }

  @Test
  public void shouldNotSetSeasonWhenMatchDateTimeIsNull() {
    FootballResult footballResult = new FootballResult(); // Assuming default matchDateTime is null
    mapperHelper.setSeason(footballResult);
    assertThat(footballResult.getSeason()).isNull();
  }

  @Test
  public void shouldSetSeasonWhenMatchDateTimeIsNotNull() {
    LocalDateTime matchDateTime = LocalDateTime.now(); // or some specific test date
    FootballResult footballResult = new FootballResult();
    footballResult.setMatchDateTime(matchDateTime);
    mapperHelper.setSeason(footballResult);
    assertThat(footballResult.getSeason()).isEqualTo(SeasonHelper.calculateSeason(matchDateTime));
  }

  private FootballResult createSampleFootballResult(LocalDateTime matchDateTime) {

    if(matchDateTime == null) {
      matchDateTime = LocalDateTime.now().withYear(2021).withMonth(10);
    }

    FootballResult footballResult = new FootballResult();
    footballResult.setId(1L);
    footballResult.setSeason("21/22");
    footballResult.setLocation("Stadium1");
    footballResult.setHomeTeam("HomeTeam1");
    footballResult.setAwayTeam("AwayTeam1");
    footballResult.setHomeScore(2);
    footballResult.setAwayScore(1);
    footballResult.setMatchDateTime(matchDateTime);
    return footballResult;
  }

  private FootballResultDTO createSampleFootballResultDTO(LocalDateTime matchDateTime) {

    if(matchDateTime == null) {
      matchDateTime = LocalDateTime.now().withYear(2021).withMonth(10);
    }

    return FootballResultDTO.of(
        "Stadium1",
        "HomeTeam1",
        "AwayTeam1",
        2,
        1,
        matchDateTime
    );
  }

  private FootballResultJsonDTO createSampleFootballResultJsonDTO() {
    return FootballResultJsonDTO.builder()
        .location("Stadium1")
        .homeTeam("HomeTeam1")
        .awayTeam("AwayTeam1")
        .homeTeamScore(2)
        .awayTeamScore(1)
        .dateUtc("2023-12-19T17:25:46.087902")
        .build();
  }
}
