package com.mojosoft.demo.controller;

import com.mojosoft.demo.service.FootballResultSpringReadWriteThroughService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ExtendWith(MockitoExtension.class)
class FootballResultSpringReadWriteThroughControllerTest {

  @Mock
  private FootballResultSpringReadWriteThroughService footballResultService;

  @InjectMocks
  protected FootballResultSpringReadWriteThroughController footballResultController;

  private FootballResultControllerTestHelper helper;

  @BeforeEach
  void setup() {
    MockMvc mockMvc = standaloneSetup(footballResultController).build();
    String baseUrl = "/api/v1/football-results/spring-readwrite-through";
    helper = new FootballResultControllerTestHelper(baseUrl, footballResultService, footballResultController, mockMvc);
  }

  @Test
  @DisplayName("Test getAll Football Results (Spring Read Through)")
  void testGetAllFootballResults() throws Exception {
    helper.assertGetAllFootballResults();
  }

  @Test
  @DisplayName("Test getFootballResultsBySeason (Spring Read Through)")
  void testGetFootballResultsBySeason() throws Exception {
    helper.assertGetFootballResultsBySeason();
  }

  @Test
  @DisplayName("Test getFootballResultsByMatch (Spring Read Through)")
  void getFootballResultByMatch() throws Exception {
    helper.assertGetFootballResultByMatch();
  }

  @Test
  @DisplayName("Test uploading football results without cache (Spring Read Through)")
  void uploadFootballResultsNoCache() throws Exception {
    helper.assertUploadFootballResults();
  }
}
