package com.mojosoft.demo.controller;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

import com.mojosoft.demo.service.FootballResultNoCacheService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(MockitoExtension.class)
class FootballResultNoCacheControllerTest {

  @Mock
  private FootballResultNoCacheService footballResultService;

  @InjectMocks
  protected FootballResultNoCacheController footballResultController;

  private FootballResultControllerTestHelper helper;

  @BeforeEach
  void setup() {
    MockMvc mockMvc = standaloneSetup(footballResultController).build();
    String baseUrl = "/api/v1/football-results/no-cache";
    helper = new FootballResultControllerTestHelper(baseUrl, footballResultService, footballResultController, mockMvc);
  }

  @Test
  @DisplayName("Test getAll Football Results")
  void testGetAllFootballResults() throws Exception {
    helper.assertGetAllFootballResults();
  }

  @Test
  @DisplayName("Test getFootballResultsBySeason")
  void testGetFootballResultsBySeason() throws Exception {
    helper.assertGetFootballResultsBySeason();
  }

  @Test
  @DisplayName("Test getFootballResultsByMatch")
  void getFootballResultByMatch() throws Exception {
    helper.assertGetFootballResultByMatch();
  }

  @Test
  @DisplayName("Test uploading football results without cache")
  void uploadFootballResultsNoCache() throws Exception {
    helper.assertUploadFootballResults();
  }
}
