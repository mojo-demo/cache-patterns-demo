package com.mojosoft.demo.controller;

import com.mojosoft.demo.controller.base.FootballResultControllerBase;
import com.mojosoft.demo.service.FootballResultNoCacheService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class for testing football results without cache.
 */
@RestController
@Tag(name = "Football Results (No Cache)")
@RequestMapping("/api/v1/football-results/no-cache")
public class FootballResultNoCacheController extends FootballResultControllerBase {

  public FootballResultNoCacheController(FootballResultNoCacheService resultService) {
    super(resultService);
  }
}