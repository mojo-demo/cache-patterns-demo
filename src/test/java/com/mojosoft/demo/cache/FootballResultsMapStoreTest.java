package com.mojosoft.demo.cache;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import com.mojosoft.demo.dto.FootballResultDTO;
import com.mojosoft.demo.entity.FootballResult;
import com.mojosoft.demo.mapper.MapperHelper;
import com.mojosoft.demo.service.FootballResultDbService;
import com.mojosoft.demo.repository.FootballResultRepository;
import com.mojosoft.demo.repository.FootballResultSaveOrUpdateHelper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.*;

@ExtendWith(MockitoExtension.class)
class FootballResultsMapStoreTest {

  @Mock
  private FootballResultRepository footballResultRepository;

  @Mock
  private FootballResultDbService footballResultDbService;

  @Mock
  private FootballResultSaveOrUpdateHelper footballResultSaveOrUpdateHelper;

  @Mock
  private MapperHelper mapperHelper;

  @InjectMocks
  private FootballResultsMapStore footballResultsMapStore;

  @Test
  @DisplayName("Store a Football Result")
  void store() {
    // Arrange
    FootballResultDTO dto = getSampleFootballResult();
    doNothing().when(footballResultSaveOrUpdateHelper).save(any(), any());

    footballResultsMapStore.store(MatchKey.createKey(dto), dto);

    verify(footballResultSaveOrUpdateHelper).save(any(), any());
  }

  @Test
  @DisplayName("Store multiple Football Results")
  void storeAll() {
    FootballResultDTO sampleFootballResult1 = getSampleFootballResult();
    FootballResultDTO sampleFootballResult2 = getSampleFootballResult2();
    String key1 = MatchKey.createKey(sampleFootballResult1);
    String key2 = MatchKey.createKey(sampleFootballResult2);

    Map<String, FootballResultDTO> dtos = new HashMap<>();
    dtos.put(key1, sampleFootballResult1);
    dtos.put(key2, sampleFootballResult2);

    when(footballResultSaveOrUpdateHelper.saveAll(any())).thenReturn(new ArrayList<>(dtos.values()));

    footballResultsMapStore.storeAll(dtos);

    verify(footballResultSaveOrUpdateHelper).saveAll(any());
  }

  @Test
  @DisplayName("Should not store multiple Football Results when map is null")
  public void testStoreAllNotCallSaveOrUpdateHelperWhenMapIsNull() {
    footballResultsMapStore.storeAll(null);
    verify(footballResultSaveOrUpdateHelper, times(0)).saveAll(any());
  }

  @Test
  @DisplayName("Should not store multiple Football Results when map is empty")
  public void testStoreAllNotCallSaveOrUpdateHelperWhenMapIsEmpty() {
    footballResultsMapStore.storeAll(Collections.emptyMap());
    verify(footballResultSaveOrUpdateHelper, times(0)).saveAll(any());
  }

  @Test
  @DisplayName("Delete a Football Result by Key")
  void delete() {
    FootballResultDTO sampleFootballResult1 = getSampleFootballResult();
    String key = MatchKey.createKey(sampleFootballResult1);
    footballResultsMapStore.delete(key);
    verify(footballResultRepository).deleteBySeasonAndHomeTeamAndAwayTeam(anyString(), anyString(), anyString());
  }

  @Test
  @DisplayName("Delete multiple Football Results")
  void deleteAll() {
    FootballResultDTO sampleFootballResult1 = getSampleFootballResult();
    FootballResultDTO sampleFootballResult2 = getSampleFootballResult2();
    String key1 = MatchKey.createKey(sampleFootballResult1);
    String key2 = MatchKey.createKey(sampleFootballResult2);

    Collection<String> keys = List.of(key1, key2);
    footballResultsMapStore.deleteAll(keys);
    verify(footballResultDbService).deleteAllByMatchKeys(anyList());
  }

  @Test
  @DisplayName("Load a Football Result by Key")
  void testLoad() {
    FootballResultDTO sampleFootballResult1 = getSampleFootballResult();
    String key = MatchKey.createKey(sampleFootballResult1);

    FootballResult entity = convertDto2Entity(sampleFootballResult1);

    when(footballResultRepository.findBySeasonAndHomeTeamAndAwayTeam(anyString(), anyString(), anyString()))
        .thenReturn(Optional.of(entity));
    when(mapperHelper.footballResultToDto(entity)).thenReturn(sampleFootballResult1);

    FootballResultDTO result = footballResultsMapStore.load(key);

    assertThat(result).isEqualTo(sampleFootballResult1);
  }

  @Test
  @DisplayName("Load a Football Result by Key should return null when result not present")
  public void testLoadShouldReturnNullDtoWhenResultNotPresent() {
    FootballResultDTO sampleFootballResult1 = getSampleFootballResult();
    String key = MatchKey.createKey(sampleFootballResult1);

    when(footballResultRepository.findBySeasonAndHomeTeamAndAwayTeam(any(), any(), any()))
        .thenReturn(Optional.empty());

    FootballResultDTO result = footballResultsMapStore.load(key);

    assertThat(result).isNull();
    verify(mapperHelper).footballResultToDto(null);
  }

  @Test
  @DisplayName("Load multiple Football Results")
  void loadAll() {
    FootballResultDTO sampleFootballResult1 = getSampleFootballResult();
    FootballResultDTO sampleFootballResult2 = getSampleFootballResult2();
    String key1 = MatchKey.createKey(sampleFootballResult1);
    String key2 = MatchKey.createKey(sampleFootballResult2);

    Collection<String> keys = List.of(key1, key2);
    FootballResult entity = new FootballResult();
    Map<String, FootballResultDTO> expectedResults = new HashMap<>();
    expectedResults.put(key1, sampleFootballResult1);
    expectedResults.put(key2, sampleFootballResult2);

    when(footballResultRepository.findBySeasonAndHomeTeamAndAwayTeam(anyString(), anyString(), anyString()))
        .thenReturn(Optional.of(entity));

    when(mapperHelper.footballResultToDto(entity))
        .thenReturn(sampleFootballResult1)
        .thenReturn(sampleFootballResult2);

    Map<String, FootballResultDTO> results = footballResultsMapStore.loadAll(keys);

    assertThat(results).isEqualTo(expectedResults);
  }

  @Test
  @DisplayName("Load all Football Result keys")
  void loadAllKeys() {
    FootballResultDTO sampleFootballResult1 = getSampleFootballResult();
    FootballResultDTO sampleFootballResult2 = getSampleFootballResult2();
    String key1 = MatchKey.createKey(sampleFootballResult1);
    String key2 = MatchKey.createKey(sampleFootballResult2);

    List<String> expectedKeys = List.of(key1, key2);
    when(footballResultRepository.findAllMatchKeys()).thenReturn(expectedKeys);

    Iterable<String> resultKeys = footballResultsMapStore.loadAllKeys();

    assertThat(resultKeys).isEqualTo(expectedKeys);
  }

  private FootballResult convertDto2Entity(FootballResultDTO dto) {
    FootballResult entity = new FootballResult();
    entity.setSeason(dto.season());
    entity.setHomeTeam(dto.homeTeam());
    entity.setAwayTeam(dto.awayTeam());
    entity.setHomeScore(dto.homeScore());
    entity.setAwayScore(dto.awayScore());
    entity.setMatchDateTime(dto.matchDateTime());
    return entity;
  }

  private FootballResultDTO getSampleFootballResult() {
    return new FootballResultDTO("21/22", "Stadium1", "HomeTeam1", "AwayTeam1", 2, 1, LocalDateTime.now());
  }

  private FootballResultDTO getSampleFootballResult2() {
    return new FootballResultDTO("21/22", "Stadium2", "HomeTeam2", "AwayTeam2", 1, 1, LocalDateTime.now());
  }

}