package com.mojosoft.demo.common;

import static org.assertj.core.api.Assertions.assertThat;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import com.mojosoft.demo.utils.DatabaseHelper;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import java.util.Collection;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

/**
 * This class contains common data management steps.
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Slf4j
public class DataManager {

  @Autowired
  private DatabaseHelper databaseHelper;

  @Autowired
  private CacheManager cacheManager;

  @Autowired
  private HazelcastInstance hazelcastInstance;

  @Then("clear all persisted data")
  public void clearAllPersistedData() {
    databaseHelper.resetAll();
  }

  /**
   * This method clears all persisted data.
   */
  @Then("clear all cached data")
  public void clearAllCachedData() {
    Collection<String> cacheNames = cacheManager.getCacheNames();
    for (String cacheName : cacheNames) {
      Cache cache = cacheManager.getCache(cacheName);
      IMap<Object, Object> map = hazelcastInstance.getMap(cacheName);
      log.info("Cache name: {} and size {}", cacheName, map.size());
      if (cache != null) {
        cache.clear();
      }
    }
  }

  /**
   * This method checks all cached data.
   */
  @Then("check cache data")
  public void checkCachedData() {
    Collection<String> cacheNames = cacheManager.getCacheNames();
    for (String cacheName : cacheNames) {
      IMap<Object, Object> map = hazelcastInstance.getMap(cacheName);
      //LocalMapStats mapStats = map.getLocalMapStats();
      log.info("Cache name: {} and size {}", cacheName, map.size());
    }
  }

  /**
   * This method checks the given cache is not empty.
   *
   * @param cacheName the cache name
   */
  @And("the {string} is not empty")
  public void theFootballResultsCacheIsNotEmpty(String cacheName) {
    IMap<Object, Object> map = hazelcastInstance.getMap(cacheName);
    assertThat(map.size()).isGreaterThan(0);
  }
}
