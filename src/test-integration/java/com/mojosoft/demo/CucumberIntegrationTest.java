package com.mojosoft.demo;

import static io.cucumber.junit.platform.engine.Constants.GLUE_PROPERTY_NAME;
import static io.cucumber.junit.platform.engine.Constants.PLUGIN_PROPERTY_NAME;

import io.cucumber.spring.CucumberContextConfiguration;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.junit.platform.suite.api.ConfigurationParameter;
import org.junit.platform.suite.api.IncludeEngines;
import org.junit.platform.suite.api.SelectClasspathResource;
import org.junit.platform.suite.api.Suite;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * This class is used to run cucumber tests.
 */
@Suite
@IncludeEngines("cucumber")
@SelectClasspathResource("cucumber")
@ConfigurationParameter(key = PLUGIN_PROPERTY_NAME,
    value = "pretty, junit:build/reports/cucumber/report.xml, "
        + "json:build/reports/cucumber/report.json, "
        + "html:build/cucumberHtml/index.html")
@ConfigurationParameter(key = GLUE_PROPERTY_NAME, value = "com.mojosoft.demo")
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Testcontainers
@Execution(ExecutionMode.SAME_THREAD)
@ActiveProfiles("test")
@CucumberContextConfiguration
@AutoConfigureWebTestClient(timeout = "36000")
public class CucumberIntegrationTest {
}