package com.mojosoft.demo.service.base;

import com.mojosoft.demo.dto.FootballResultDTO;
import com.mojosoft.demo.dto.FootballResultJsonDTO;
import com.mojosoft.demo.dto.UploadResponseDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service interface for managing football results.
 */
@SuppressWarnings("all")
public interface FootballResultServiceI {

  /**
   * This method saves the football results.
   *
   * @param results the list of {@link FootballResultJsonDTO} objects
   * @return the {@link UploadResponseDTO} object
   */
  public UploadResponseDTO save(List<FootballResultJsonDTO> results);

  public List<FootballResultDTO> getAll();

  /**
   * This method returns the football results by season.
   *
   * @param season the season
   * @return the list of {@link FootballResultDTO} objects
   */
  public List<FootballResultDTO> getBySeason(String season);

  /**
   * This method returns the football results by season and teams.
   *
   * @param season the season
   * @param homeTeam the home team
   * @param awayTeam the away team
   * @return the {@link FootballResultDTO} object
   */
  public Optional<FootballResultDTO> getByMatch(String season, String homeTeam, String awayTeam);

}
