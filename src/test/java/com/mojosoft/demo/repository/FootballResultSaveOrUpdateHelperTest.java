package com.mojosoft.demo.repository;

import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.*;

import com.mojosoft.demo.cache.MatchKey;
import com.mojosoft.demo.dto.FootballResultDTO;
import com.mojosoft.demo.entity.FootballResult;
import com.mojosoft.demo.mapper.MapperHelper;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.extension.ExtendWith;

/**
 * This class tests the FootballResultSaveOrUpdateHelper class.
 */
@ExtendWith(MockitoExtension.class)
class FootballResultSaveOrUpdateHelperTest {

  @Mock
  private FootballResultRepository footballResultRepository;

  @Mock
  private MapperHelper mapperHelper;

  @InjectMocks
  private FootballResultSaveOrUpdateHelper saveOrUpdateHelper;

  @Test
  @DisplayName("Test saving a single football result")
  void testSave() {
    MatchKey key = new MatchKey("2020", "HomeTeam", "AwayTeam");
    FootballResultDTO dto = getSampleFootballResultDTO(); // set necessary fields
    FootballResult entity = convertDto2Entity(dto); // set necessary fields

    when(mapperHelper.footballResultToEntity(dto)).thenReturn(entity);
    when(footballResultRepository.findBySeasonAndHomeTeamAndAwayTeam(key.season(), key.homeTeam(), key.awayTeam()))
        .thenReturn(Optional.empty());

    saveOrUpdateHelper.save(key, dto);

    verify(footballResultRepository).save(any(FootballResult.class));
  }

  @Test
  @DisplayName("Test saving a list of football results")
  void testSaveAll() {
    FootballResultDTO dto1 = getSampleFootballResultDTO();
    FootballResultDTO dto2 = getSampleFootballResultDTO();
    List<FootballResultDTO> dtos = List.of(dto1, dto2);

    FootballResult entity1 = convertDto2Entity(dto1);
    FootballResult entity2 = convertDto2Entity(dto2);

    when(mapperHelper.footballResultToEntity(any(FootballResultDTO.class))).thenReturn(entity1, entity2);
    when(footballResultRepository.saveAll(anyList())).thenReturn(List.of(entity1, entity2));
    when(mapperHelper.footballResultToDto(any(FootballResult.class))).thenReturn(dto1, dto2);

    List<FootballResultDTO> savedDtos = saveOrUpdateHelper.saveAll(dtos);

    verify(footballResultRepository).saveAll(anyList());
    assertThat(savedDtos).hasSize(dtos.size());
    assertThat(savedDtos).containsExactlyInAnyOrderElementsOf(dtos);
  }

  @Test
  @DisplayName("Test saving a list of football results when input is empty")
  public void testSaveReturnEmptyListWhenInputIsEmpty() {
    List<FootballResultDTO> result = saveOrUpdateHelper.saveAll(List.of());
    assertThat(result).isEmpty();
  }

  private FootballResult convertDto2Entity(FootballResultDTO dto) {
    FootballResult entity = new FootballResult();
    entity.setSeason(dto.season());
    entity.setHomeTeam(dto.homeTeam());
    entity.setAwayTeam(dto.awayTeam());
    entity.setHomeScore(dto.homeScore());
    entity.setAwayScore(dto.awayScore());
    entity.setMatchDateTime(dto.matchDateTime());
    return entity;
  }

  private FootballResultDTO getSampleFootballResultDTO() {
    return new FootballResultDTO("21/22", "Stadium1", "HomeTeam1", "AwayTeam1", 2, 1, LocalDateTime.now());
  }
}
