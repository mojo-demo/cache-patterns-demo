@READ_THROUGH_CACHE
Feature: Read Through Cache (Spring Boot Caching)

  Scenario: Uploading Season Data first and verify it is persisted (read-through cache)
    Given the system is initialized with no data
    When I upload season data from 'sample-data/epl-2020.json' to 'api/v1/football-results/spring-readwrite-through'
    Then the system returns a created status
    And the response includes the number of rows inserted
    And all the uploaded results are stored
    And the 'footballResultsCache' is not empty

  Scenario: Get all matches (Should hit cache as we are configured to use write-through)
    Given the 'footballResultsCache' cache hit count is noted
    When I request all matches using the 'api/v1/football-results/spring-readwrite-through' endpoint
    Then the system returns a success status
    And the response should contain a list of matches
    And the number of matches in the response should be the same as the number uploaded
    And the cache hit count should have increased by 1 since the last note

  Scenario: Getting Matches by Season (read-through cache)
    Given the 'footballResultsCache' cache hit count is noted
    When I request matches for season '20/21' using the 'api/v1/football-results/spring-readwrite-through/season/' endpoint
    Then the system returns a success status
    And the response should only contain season '20/21'
    And the number of matches in the response should be correct for season '20/21'
    And the cache hit count should have increased by 0 since the last note
    When I request matches for season '20/21' using the 'api/v1/football-results/spring-readwrite-through/season/' endpoint
    Then the cache hit count should have increased by 1 since the last note

  Scenario: Getting a Single Match (read-through cache)
    Given the 'footballResultsCache' cache hit count is noted
    When I request a match by teams using the 'api/v1/football-results/spring-readwrite-through/season/20%2F21/home_team/Fulham/away_team/Crystal+Palace' endpoint
    Then the system returns a success status
    And the match returned should be 'Fulham' V 'Crystal Palace' in '20/21'
    And the cache hit count should have increased by 0 since the last note
    When I request a match by teams using the 'api/v1/football-results/spring-readwrite-through/season/20%2F21/home_team/Fulham/away_team/Crystal+Palace' endpoint
    Then the cache hit count should have increased by 1 since the last note

  Scenario: Finally clear persisted data
    Then clear all persisted data