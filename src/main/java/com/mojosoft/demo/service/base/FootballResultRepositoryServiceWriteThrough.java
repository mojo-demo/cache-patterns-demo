package com.mojosoft.demo.service.base;

import com.mojosoft.demo.dto.FootballResultDTO;
import com.mojosoft.demo.repository.FootballResultSaveOrUpdateHelper;
import com.mojosoft.demo.service.FootballResultSpringReadWriteThroughService;
import java.util.List;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

/**
 * Service class for write-through caching football results.
 */
@Service
public class FootballResultRepositoryServiceWriteThrough {

  private final FootballResultSaveOrUpdateHelper footballResultSaveOrUpdateHelper;

  public FootballResultRepositoryServiceWriteThrough(
      FootballResultSaveOrUpdateHelper footballResultSaveOrUpdateHelper) {
    this.footballResultSaveOrUpdateHelper = footballResultSaveOrUpdateHelper;
  }

  /**
   * This method saves the football results.
   *
   * @param results the football results
   * @return the list of {@link FootballResultDTO} objects
   */
  @CachePut(
      value = FootballResultSpringReadWriteThroughService.FOOTBALL_RESULTS_CACHE,
      key = "'all'")
  public List<FootballResultDTO> save(List<FootballResultDTO> results) {
    return footballResultSaveOrUpdateHelper.saveAll(results);
  }
}
