package com.mojosoft.demo.cache;

import com.mojosoft.demo.dto.FootballResultDTO;

/**
 * A composite key for a match.
 */
public record MatchKey(String season, String homeTeam, String awayTeam) {

  /**
   * Creates a composite key for a match.
   *
   * @param season The season of the match.
   * @param homeTeam The home team.
   * @param awayTeam The away team.
   * @return A composite key in the format "season_homeTeam_awayTeam".
   */
  public static String createKey(String season, String homeTeam, String awayTeam) {
    return season + "_" + homeTeam + "_" + awayTeam;
  }

  /**
   * Creates a composite key for a match.
   *
   * @param footballResultDTO The football result DTO.
   * @return A composite key in the format "season_homeTeam_awayTeam".
   */
  @SuppressWarnings("checkstyle:AbbreviationAsWordInName")
  public static String createKey(FootballResultDTO footballResultDTO) {
    return footballResultDTO.season() + "_"
        + footballResultDTO.homeTeam() + "_"
        + footballResultDTO.awayTeam();
  }

  /**
   * Creates a composite matchkey for a match.
   *
   * @param footballResultDTO The football result DTO.
   * @return A composite key in the format "season_homeTeam_awayTeam".
   */
  @SuppressWarnings("checkstyle:AbbreviationAsWordInName")
  public static MatchKey createMatchKey(FootballResultDTO footballResultDTO) {
    return new MatchKey(
        footballResultDTO.season(),
        footballResultDTO.homeTeam(),
        footballResultDTO.awayTeam());
  }

  /**
   * Extracts the components from a composite match key.
   *
   * @param key The composite key in the format "season_homeTeam_awayTeam".
   * @return A MatchKey record with the extracted components.
   * @throws IllegalArgumentException if the key format is invalid.
   */
  public static MatchKey extractKeyComponents(String key) {
    String[] parts = key.split("_");
    if (parts.length != 3) {
      throw new IllegalArgumentException("Invalid key format");
    }
    return new MatchKey(parts[0], parts[1], parts[2]);
  }
}
