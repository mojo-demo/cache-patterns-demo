package com.mojosoft.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This class is used to represent a football match in JSON format.
 */
@SuppressWarnings("checkstyle:AbbreviationAsWordInName")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class FootballResultJsonDTO {
  @JsonProperty("MatchNumber")
  @NotNull
  private Long matchNumber;

  @JsonProperty("RoundNumber")
  private Long roundNumber;

  @JsonProperty("DateUtc")
  @NotNull
  private String dateUtc;

  @JsonProperty("Location")
  @NotNull
  private String location;

  @JsonProperty("HomeTeam")
  @NotNull
  private String homeTeam;

  @JsonProperty("AwayTeam")
  @NotNull
  private String awayTeam;

  @JsonProperty("Group")
  private String group;

  @JsonProperty("HomeTeamScore")
  private int homeTeamScore;

  @JsonProperty("AwayTeamScore")
  private int awayTeamScore;
}