package com.mojosoft.demo.controller;

import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mojosoft.demo.controller.base.FootballResultControllerBase;
import com.mojosoft.demo.dto.FootballResultDTO;
import com.mojosoft.demo.dto.FootballResultJsonDTO;
import com.mojosoft.demo.dto.UploadResponseDTO;
import com.mojosoft.demo.service.base.FootballResultServiceI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Helper class for testing {@link FootballResultControllerBase} implementations.
 */
class FootballResultControllerTestHelper {

  protected final FootballResultServiceI footballResultService;
  protected final FootballResultControllerBase footballResultController;
  protected final String baseUrl;
  protected final MockMvc mockMvc;
  protected final ObjectMapper objectMapper = new ObjectMapper();

  public FootballResultControllerTestHelper(String baseUrl,
                                            FootballResultServiceI footballResultService,
                                            FootballResultControllerBase footballResultController,
                                            MockMvc mockMvc) {
    this.baseUrl = baseUrl;
    this.footballResultService = footballResultService;
    this.footballResultController = footballResultController;
    this.mockMvc = mockMvc;

    JavaTimeModule module = new JavaTimeModule();
    objectMapper.registerModule(module);
  }


  void assertGetAllFootballResults() throws Exception {
    List<FootballResultDTO> mockResults = getSampleFootballResultsDTO();

    when(footballResultService.getAll()).thenReturn(mockResults);

    mockMvc.perform(get(baseUrl+"/"))
        .andExpect(status().isOk())
        .andExpect(content().contentType("application/json"))
        .andExpect(jsonPath("$.size()").value(mockResults.size()))
        .andExpect(jsonPath("$[0].season").value(mockResults.get(0).season()));

    verify(footballResultService, times(1)).getAll();
  }

  void assertGetFootballResultsBySeason() throws Exception {
    String season = "20/21";
    String encodedSeason = URLEncoder.encode(season, StandardCharsets.UTF_8);

    List<FootballResultDTO> mockResults = getSampleFootballResultsDTO();

    when(footballResultService.getBySeason(season)).thenReturn(mockResults);

    mockMvc.perform(get(baseUrl+"/season/{season}", encodedSeason))
        .andExpect(status().isOk())
        .andExpect(content().json(objectMapper.writeValueAsString(mockResults)));
  }

  void assertGetFootballResultByMatch() throws Exception {
    String season = "21/22";
    String encodedSeason = URLEncoder.encode(season, StandardCharsets.UTF_8);
    String homeTeam = "HomeTeam";
    String awayTeam = "AwayTeam";
    FootballResultDTO mockResult = new FootballResultDTO(
        "21/22", "Stadium", homeTeam, awayTeam, 2, 1, LocalDateTime.now()
    );

    when(footballResultService.getByMatch(season, homeTeam, awayTeam)).thenReturn(Optional.of(mockResult));

    mockMvc.perform(get(baseUrl+"/season/{season}/home_team/{homeTeam}/away_team/{awayTeam}",
            encodedSeason, homeTeam, awayTeam))
        .andExpect(status().isOk())
        .andExpect(content().contentType("application/json"))
        .andExpect(jsonPath("$.season").value(mockResult.season()))
        .andExpect(jsonPath("$.homeTeam").value(mockResult.homeTeam()))
        .andExpect(jsonPath("$.awayTeam").value(mockResult.awayTeam()));

    verify(footballResultService, times(1)).getByMatch(season, homeTeam, awayTeam);
  }

  void assertUploadFootballResults() throws Exception {
    List<FootballResultJsonDTO> mockResults = getSampleFootballResultsJson();
    int recordsProcessed = mockResults.size();
    String successMessage = "Successfully uploaded " + recordsProcessed + " football results.";

    UploadResponseDTO mockResponse = new UploadResponseDTO(recordsProcessed, successMessage, null);
    when(footballResultService.save(anyList())).thenReturn(mockResponse);

    mockMvc.perform(put(baseUrl)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(mockResults)))
        .andExpect(status().isCreated())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.recordsProcessed").value(recordsProcessed))
        .andExpect(jsonPath("$.message").value(successMessage))
        .andExpect(jsonPath("$.error").doesNotExist());

    verify(footballResultService, times(1)).save(anyList());
  }

  private List<FootballResultDTO> getSampleFootballResultsDTO() {
    return List.of(
        new FootballResultDTO("21/22", "Stadium1", "HomeTeam1", "AwayTeam1", 2, 1, LocalDateTime.now()),
        new FootballResultDTO("21/22", "Stadium2", "HomeTeam2", "AwayTeam2", 1, 2, LocalDateTime.now())
    );
  }

  private List<FootballResultJsonDTO> getSampleFootballResultsJson() {
    return List.of(
        FootballResultJsonDTO.builder()
            .matchNumber(1L)
            .dateUtc("2023-12-19T17:25:46.087902")
            .location("Stadium1")
            .homeTeam("HomeTeam1")
            .awayTeam("AwayTeam1")
            .homeTeamScore(2)
            .awayTeamScore(1)
            .build(),
        FootballResultJsonDTO.builder()
            .matchNumber(2L)
            .dateUtc("2023-12-19T17:25:46.087902")
            .location("Stadium2")
            .homeTeam("HomeTeam2")
            .awayTeam("AwayTeam2")
            .homeTeamScore(1)
            .awayTeamScore(2)
            .build()
    );
  }
}
