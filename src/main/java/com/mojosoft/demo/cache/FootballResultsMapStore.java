package com.mojosoft.demo.cache;

import com.hazelcast.map.MapStore;
import com.mojosoft.demo.dto.FootballResultDTO;
import com.mojosoft.demo.entity.FootballResult;
import com.mojosoft.demo.mapper.MapperHelper;
import com.mojosoft.demo.repository.FootballResultRepository;
import com.mojosoft.demo.repository.FootballResultSaveOrUpdateHelper;
import com.mojosoft.demo.service.FootballResultDbService;
import jakarta.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * This class implements the {@link MapStore} interface for the {@link FootballResult} entity.
 */
@Component
public class FootballResultsMapStore
    implements MapStore<String, FootballResultDTO> {

  private final FootballResultRepository footballResultRepository;
  private final FootballResultDbService footballResultDbService;
  private final MapperHelper mapperHelper;

  private final FootballResultSaveOrUpdateHelper footballResultSaveOrUpdateHelper;

  /**
   * Constructor, used by Lazy inject the dependencies. so it occurs only when the bean
   * is used and after hazelcast is started.
   *
   * @param footballResultSaveOrUpdateHelper The save or update helper
   * @param footballResultRepository The football result repository
   * @param mapperHelper The mapper helper
   */
  @SuppressWarnings("all")
  public FootballResultsMapStore(
      @Lazy FootballResultRepository footballResultRepository,
      @Lazy FootballResultDbService footballResultDbService,
      @Lazy FootballResultSaveOrUpdateHelper footballResultSaveOrUpdateHelper,
      MapperHelper mapperHelper) {
    this.footballResultRepository = footballResultRepository;
    this.footballResultDbService = footballResultDbService;
    this.footballResultSaveOrUpdateHelper = footballResultSaveOrUpdateHelper;
    this.mapperHelper = mapperHelper;
  }

  @Override
  public void store(String key, FootballResultDTO value) {
    footballResultSaveOrUpdateHelper.save(MatchKey.extractKeyComponents(key), value);
  }

  @Override
  public void storeAll(Map<String, FootballResultDTO> map) {
    if (map == null || map.isEmpty()) {
      return;
    }
    footballResultSaveOrUpdateHelper.saveAll(new ArrayList<>(map.values()));
  }

  @Override
  @Transactional
  public void delete(String key) {
    MatchKey matchKey = MatchKey.extractKeyComponents(key);
    footballResultRepository.deleteBySeasonAndHomeTeamAndAwayTeam(
        matchKey.season(), matchKey.homeTeam(), matchKey.awayTeam());
  }

  @Override
  public void deleteAll(Collection<String> keys) {
    List<MatchKey> matchKeys = convertToMatchKey(keys);
    footballResultDbService.deleteAllByMatchKeys(matchKeys);
  }

  @Override
  public FootballResultDTO load(String key) {
    MatchKey matchKey = MatchKey.extractKeyComponents(key);
    Optional<FootballResult> footballResult = footballResultRepository
        .findBySeasonAndHomeTeamAndAwayTeam(
            matchKey.season(), matchKey.homeTeam(), matchKey.awayTeam());
    return mapperHelper.footballResultToDto(footballResult.orElse(null));
  }

  @Override
  public Map<String, FootballResultDTO> loadAll(Collection<String> keys) {
    Map<String, FootballResultDTO> results = new HashMap<>();
    keys.forEach(key -> {
      MatchKey matchKey = MatchKey.extractKeyComponents(key);
      Optional<FootballResult> footballResult =
          footballResultRepository.findBySeasonAndHomeTeamAndAwayTeam(
              matchKey.season(), matchKey.homeTeam(), matchKey.awayTeam());

      if (footballResult.isPresent()) {
        results.put(key, mapperHelper.footballResultToDto(footballResult.get()));
      } else {
        results.put(key, null);
      }
    });
    return results;
  }

  @Override
  public Iterable<String> loadAllKeys() {
    return footballResultRepository.findAllMatchKeys();
  }

  /**
   * Converts a collection of keys to a list of {@link MatchKey} objects.
   *
   * @param keys the collection of keys
   * @return a list of {@link MatchKey} objects
   */
  private List<MatchKey> convertToMatchKey(Collection<String> keys) {
    return keys.stream().map(MatchKey::extractKeyComponents).toList();
  }
}
