package com.mojosoft.demo.upload;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mojosoft.demo.common.ResponseMapper;
import com.mojosoft.demo.dto.FootballResultJsonDTO;
import com.mojosoft.demo.dto.UploadResponseDTO;
import com.mojosoft.demo.entity.FootballResult;
import com.mojosoft.demo.mapper.MapperHelper;
import com.mojosoft.demo.repository.FootballResultRepository;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;

/**
 * This class contains the step definitions for uploading results.
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Slf4j
public class UploadResults {

  @Autowired
  private ResponseMapper responseMapper;

  @Autowired
  private ResourceLoader resourceLoader;

  @Autowired
  private FootballResultRepository footballResultRepository;

  @Autowired
  private TestRestTemplate restTemplate;

  @Autowired
  private MapperHelper mapper;

  /**
   * This method uploads the season data from the sample data file to the endpoint.
   *
   * @param filePath the file path
   * @param urlPath the url path
   * @throws IOException if an I/O error occurs
   */
  @Given("I upload season data from {string} to {string}")
  public void uploadSeasonDataFromSampleDataEplJsonToSomeEndpoint(
      String filePath, String urlPath) throws IOException {
    Resource resource = resourceLoader.getResource("classpath:" + filePath);
    assertThat(resource).isNotNull();

    String resultDataJson = loadFileContent(resource);

    ObjectMapper objectMapper = new ObjectMapper();
    List<FootballResultJsonDTO> footballResults = objectMapper.readValue(
        resultDataJson, new TypeReference<List<FootballResultJsonDTO>>() {});
    responseMapper.setUploadedFootballResults(footballResults);

    assertThat(resultDataJson).isNotNull();

    String baseUrl = restTemplate.getRootUri();
    String url = baseUrl + urlPath;

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<String> requestEntity = new HttpEntity<>(resultDataJson, headers);

    ResponseEntity<UploadResponseDTO> responseEntity = restTemplate.exchange(
        url,
        HttpMethod.PUT,
        requestEntity,
        UploadResponseDTO.class);

    assertThat(responseEntity).isNotNull();
    responseMapper.setResponseEntity(responseEntity);
  }

  /**
   * Check that the response includes the number of rows inserted.
   */
  @SuppressWarnings("checkstyle:WhitespaceAfter")
  @And("the response includes the number of rows inserted")
  public void theResponseIncludesTheNumberOfRowsInserted() {
    int expectedRowsInserted = responseMapper.getUploadedFootballResults().size();

    ResponseEntity<UploadResponseDTO> responseEntity =
        (ResponseEntity<UploadResponseDTO>)responseMapper.getResponseEntity();

    assertThat(responseEntity).isNotNull();

    UploadResponseDTO response = responseEntity.getBody();

    assertThat(response)
        .as("Check if response is not null")
        .isNotNull();
    assertThat(response.getRecordsProcessed())
        .as("Check if rows processed matches expected value, expected: %d, actual: %d",
            expectedRowsInserted, response.getRecordsProcessed())
        .isEqualTo(expectedRowsInserted);
  }

  /**
   * Check that the response includes the number of rows inserted.
   */
  @And("all the uploaded results are stored")
  public void theResultsAreSavedToTheDatastore() {
    List<FootballResult> expected = responseMapper.getUploadedFootballResults()
        .stream().map(jsonDto -> mapper.footballMatchJsonToEntity(jsonDto)).toList();
    List<FootballResult> actual = footballResultRepository.findAll();

    Comparator<FootballResult> comparator = (footballResult1, footballResult2) -> {
      if (footballResult1 == footballResult2) {
        return 0;
      }
      if (footballResult1 == null) {
        return -1;
      }
      if (footballResult2 == null) {
        return 1;
      }
      if (!footballResult1.equals(footballResult2)) {
        RecursiveComparisonConfiguration recursiveComparisonConfiguration =
            new RecursiveComparisonConfiguration();
        recursiveComparisonConfiguration.ignoreFields("id");

        assertThat(footballResult1)
            .usingRecursiveComparison(recursiveComparisonConfiguration)
            .isEqualTo(footballResult2);
      }
      return 0;
    };

    List<FootballResult> sortedExpected = expected.stream()
        .sorted(Comparator.comparing(FootballResult::getHomeTeam)
            .thenComparing(FootballResult::getAwayTeam))
        .collect(Collectors.toList());

    List<FootballResult> sortedActual = actual.stream()
        .sorted(Comparator.comparing(FootballResult::getHomeTeam)
            .thenComparing(FootballResult::getAwayTeam))
        .collect(Collectors.toList());

    assertThat(sortedActual)
        .usingElementComparatorIgnoringFields("id")
        .as("Lists should match while ignoring 'id' field")
        .containsExactlyInAnyOrderElementsOf(sortedExpected);
  }

  private static String loadFileContent(Resource resource) throws IOException {
    try (InputStream inputStream = resource.getInputStream()) {
      byte[] bytes = FileCopyUtils.copyToByteArray(inputStream);
      return new String(bytes, StandardCharsets.UTF_8);
    }
  }

  @Given("the system is initialized with no data")
  public void theSystemIsInitializedWithNoData() {
  }
}
