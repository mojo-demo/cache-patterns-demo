package com.mojosoft.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.mojosoft.demo.dto.FootballResultDTO;
import com.mojosoft.demo.dto.FootballResultJsonDTO;
import com.mojosoft.demo.dto.UploadResponseDTO;
import com.mojosoft.demo.entity.FootballResult;
import com.mojosoft.demo.mapper.MapperHelper;
import com.mojosoft.demo.repository.FootballResultRepository;
import com.mojosoft.demo.repository.FootballResultSaveOrUpdateHelper;
import com.mojosoft.demo.service.base.FootballResultServiceI;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

class FootballResultServiceTestHelper {

  private final FootballResultRepository footballResultRepository;
  private final MapperHelper mapper;
  private final FootballResultServiceI footballResultService;
  private final FootballResultSaveOrUpdateHelper footballResultSaveOrUpdateHelper;

  public FootballResultServiceTestHelper(FootballResultServiceI footballResultService,
                                         FootballResultRepository footballResultRepository,
                                         FootballResultSaveOrUpdateHelper footballResultSaveOrUpdateHelper,
                                         MapperHelper mapper) {
    this.footballResultService = footballResultService;
    this.footballResultRepository = footballResultRepository;
    this.footballResultSaveOrUpdateHelper = footballResultSaveOrUpdateHelper;
    this.mapper = mapper;
  }
  void assertSaveResults() {
    List<FootballResultJsonDTO> inputResults = List.of(new FootballResultJsonDTO(), new FootballResultJsonDTO());

    when(footballResultSaveOrUpdateHelper.saveAll(any())).thenReturn(List.of(getSampleFootballResult(), getSampleFootballResult()));

    UploadResponseDTO response = footballResultService.save(inputResults);

    assertThat(response.getRecordsProcessed())
        .as("Check if records processed count is correct")
        .isEqualTo(2);
    assertThat(response.getMessage())
        .as("Check if success message is formatted correctly")
        .isEqualTo("Successfully uploaded 2 football results.");
    assertThat(response.getError())
        .as("Check if error is null")
        .isNull();

    verify(footballResultSaveOrUpdateHelper, times(1)).saveAll(any());
  }

  void assertGetAll() {
    List<FootballResult> footballResults = List.of(new FootballResult(), new FootballResult());
    when(footballResultRepository.findAll()).thenReturn(footballResults);
    when(mapper.footballResultToDto(any(FootballResult.class))).thenAnswer(invocation -> getSampleFootballResult());

    List<FootballResultDTO> resultDTOs = footballResultService.getAll();

    assertThat(resultDTOs)
        .as("Check if the retrieved list has the correct size")
        .hasSize(footballResults.size());

    verify(footballResultRepository, times(1)).findAll();
    verify(mapper, times(footballResults.size())).footballResultToDto(any(FootballResult.class));
  }

  void assertGetBySeason() {
    String season = "20/21";
    List<FootballResult> footballResults = List.of(new FootballResult(), new FootballResult());
    when(footballResultRepository.findAllBySeason(season)).thenReturn(footballResults);
    when(mapper.footballResultToDto(any(FootballResult.class))).thenAnswer(invocation -> getSampleFootballResult());

    List<FootballResultDTO> resultDTOs = footballResultService.getBySeason(season);

    assertThat(resultDTOs)
        .as("Check if the retrieved list has the correct size for the season")
        .hasSize(footballResults.size());

    verify(footballResultRepository, times(1)).findAllBySeason(season);
    verify(mapper, times(footballResults.size())).footballResultToDto(any(FootballResult.class));
  }

  void assertGetByMatch() {
    String season = "21/22";
    String homeTeam = "HomeTeam1";
    String awayTeam = "AwayTeam1";
    FootballResult footballResult = new FootballResult();
    when(footballResultRepository.findBySeasonAndHomeTeamAndAwayTeam(season, homeTeam, awayTeam)).thenReturn(Optional.of(footballResult));
    when(mapper.footballResultToDto(footballResult)).thenReturn(getSampleFootballResult());

    Optional<FootballResultDTO> resultDTO = footballResultService.getByMatch(season, homeTeam, awayTeam);

    assertThat(resultDTO)
        .as("Check if the retrieved football result DTO is not null")
        .isNotNull();

    verify(footballResultRepository, times(1)).findBySeasonAndHomeTeamAndAwayTeam(season, homeTeam, awayTeam);
    verify(mapper, times(1)).footballResultToDto(footballResult);
  }

  private FootballResultDTO getSampleFootballResult() {
    return new FootballResultDTO("21/22", "Stadium1", "HomeTeam1", "AwayTeam1", 2, 1, LocalDateTime.now());
  }
}