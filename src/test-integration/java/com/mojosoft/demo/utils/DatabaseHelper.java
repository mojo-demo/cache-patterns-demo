package com.mojosoft.demo.utils;

import com.mojosoft.demo.repository.FootballResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * This class contains helper methods for database operations.
 */
@Component
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
public class DatabaseHelper {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Autowired
  private FootballResultRepository footballResultRepository;

  public void resetAll() {
    footballResultRepository.deleteAll();
    resetSequence("football_result");
  }

  public void resetSequence(String tableName) {
    String sequenceName = tableName + "_id_seq";
    jdbcTemplate.execute("ALTER SEQUENCE " + sequenceName + " RESTART WITH 1");
  }
}
