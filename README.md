# Read/Write Through and Write Behind Cache Patterns

## Project Overview
This Java project demonstrates the implementation of Read/Write Through and Write behind Cache Patterns with a distributed cache. These patterns are commonly used in software design to enhance performance and efficiency of data access. The project showcases how these patterns can be implemented in Java applications to optimize data retrieval and storage processes.

## Features
- **Read-Through:** Automatically loads data into the cache upon a cache miss.
```mermaid
sequenceDiagram
    participant A as Application
    participant C as Cache
    participant D as Data Store

    A->>C: Request Data
    alt Cache Hit
        C->>A: Return Data from Cache
    else Cache Miss
        C->>D: Fetch Data
        D->>C: Return Data
        C->>C: Store Data in Cache
        C->>A: Return Data
    end
```

- **Write-Through:** Immediately writes data to the underlying data store and updates the cache.
```mermaid
sequenceDiagram
    participant A as Application
    participant C as Cache
    participant D as Data Store

    A->>C: Write Data
    C->>D: Write Data
    D->>C: Acknowledge Write
    C->>A: Acknowledge Write
```

- **Write-Behind:** Performs writes to the underlying data store and updates the cache asynchronously.
```mermaid
sequenceDiagram
    participant A as Application
    participant C as Cache
    participant D as Data Store

    A->>C: Write Data
    C->>A: Acknowledge Write
    Note over C: Data Temporarily Held in Cache
    C->>D: Later, Asynchronously Write Data to Data Store
    Note over D: Data Persisted to Store
```
----

### Benefits
- **Read-Through:** Improves performance by reducing the number of calls to the underlying data store.
- **Write-Through:** Ensures that the cache and data store are always in sync.
- **Write-Behind:** Improves performance by reducing the number of calls to the underlying data store.
- **Write-Behind:** Reduces the risk of data loss by persisting data to the underlying data store asynchronously.

### Disadvantages
- **Read-Through:** Data may become stale if the underlying data store is updated outside the application.
- **Write-Through:** Performance may be impacted if the underlying data store is slow.
- **Write-Behind:** Data may become stale if the underlying data store is updated outside the application.

----

### JCache/IMDG, Databases, and Spring Boot Caching: Integration and Benefits

- **JCache/IMDG Usage**
    - Ideal for **rapid, in-memory data access**, especially for key-value retrieval.
    - Reduces load on databases by caching frequently accessed "hot data".

- **Database Usage**
    - Best suited for **complex transactions** and **persistent storage**.
    - Essential for scenarios demanding data integrity and complex querying.

- **Combining JCache/IMDG with Databases**
    - JCache/IMDG for caching to enhance performance and reduce database load.
    - Databases manage persistent storage and handle complex data operations.

- **Integrating with Spring Boot Caching**
    - Spring Boot's caching abstraction backed by JCache provides a **unified caching layer**.
    - Results of methods and queries can be cached and shared across services in a distributed manner.
    - Ideal for applications with **scalable, distributed architectures** where multiple instances need access to shared cache.
    - Enhances performance by **avoiding redundant computations** and **reducing database queries**.

- **When and Why to Use Integrated Caching**
    - Use when application instances are distributed across multiple nodes and require **consistent view of cached data**.
    - Beneficial in **microservices architectures** where services can leverage shared cache for common data.
    - Useful for applications with **high read-to-write ratio**, where caching can significantly reduce database reads.

### Application-Specific Decisions
- The choice of caching strategy should be guided by **specific application requirements**.
- Consider factors like data access patterns, workload nature, and scalability needs.

----
## Todo

- **Implement ControlAdvice for Error Handling**
  - Develop a ControlAdvice mechanism to manage exceptions and errors more effectively. This will provide a centralized way to handle errors and standardize the response format for API errors.

- **Transition to JCache Standard**
  - Migrate from using Hazelcast's IMDG proprietary features to strictly using JCache standard APIs. This will enhance the portability and interoperability of the caching implementation.

- **Resolve Issues with Running All Cucumber Tests Simultaneously on pipeline**
  - Address the problems encountered when running all Cucumber tests together, investigate and fix the root cause to allow seamless execution of the entire test suite on pipeline.

- **Implement Observability and Create Grafana Dashboard**
  - Enhance the project with @Observability to monitor key metrics and performance indicators. Develop a comprehensive Grafana dashboard for real-time monitoring and visualization of these metrics.

- **Error Handling and Monitoring**
  - Ensure that there are sufficient mechanisms for error logging and monitoring. This is crucial for diagnosing issues in production and improving system reliability.

----
## Getting Started
These instructions will help you get the project up and running on your local machine for development and testing purposes.

### Prerequisites
What things you need to install and how to install them:

- Java Development Kit (JDK), version 17 or higher.
- A suitable Integrated Development Environment (IDE) like IntelliJ IDEA, Eclipse, or VSCode.
- Docker, for dependencies like databases in containers.

### Installing
A step-by-step series of examples that tell you how to get a development environment running:

1. Clone the Repository

```bash
git clone https://gitlab.com/mojo-demo/cache-patterns-demo.git
```

1. Navigate to Project Directory

```bash
cd cache-patterns-demo
```

1. Build the Project

```bash
gradle build
```
2. Run the Tests

```bash
./gradlew unitTest
./gradlew cucumber
```
----

## Usage
> The service is designed as a demonstration of the Read/Write Through and Write Behind Cache Patterns. It is reccomended that the service is used via the tests provided in the project. However, the service can also be run as a standalone application.

The service exposes a REST API for accessing and manipulating data. The API is documented using Swagger and can be accessed at:

- Swagger UI: http://localhost:8989/swagger-ui/index.html
- Swagger API Docs: http://localhost:8989/v2/api-docs
----
### Sample Data for Testing:

Is available in the following files:
- [EPL 2020](src/main/resources/sample-data/epl-2020.json)
- [EPL 2021](src/main/resources/sample-data/epl-2021.json)
- [EPL 2022](src/main/resources/sample-data/epl-2022.json)

Alternatively
- Data can also be downloaded from [fixturedownload.com](https://fixturedownload.com/feed/json/)

----
## License
This project is licensed under the MIT License.

