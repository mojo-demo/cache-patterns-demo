package com.mojosoft.demo.repository;

import com.mojosoft.demo.entity.FootballResult;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the FootballResult entity.
 */
@Repository
public interface FootballResultRepository
    extends JpaRepository<FootballResult, Long> {
  List<FootballResult> findAllBySeason(String season);

  Optional<FootballResult> findBySeasonAndHomeTeamAndAwayTeam(
      String season,
      String homeTeam,
      String awayTeam);

  void deleteBySeasonAndHomeTeamAndAwayTeam(String season, String homeTeam, String awayTeam);

  @Query(value = "SELECT fr.season || '_' || fr.home_team || '_' || fr.away_team "
      + "FROM football_result fr", nativeQuery = true)
  List<String> findAllMatchKeys();
}