FROM openjdk:17-jdk
EXPOSE 8989
ARG JAR_FILE=./build/libs/app.jar
COPY ${JAR_FILE} /home/
ENTRYPOINT ["java","-jar","./home/app.jar"]
