package com.mojosoft.demo.config;

import io.micrometer.observation.ObservationRegistry;
import io.micrometer.observation.aop.ObservedAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ObservationConfig.
 */
@Configuration(proxyBeanMethods = false)
class ObservationConfig {

  /**
   * To have the @Observed support we need to register this aspect as a bean.
   *
   * @param observationRegistry The registry to which observations will be sent.
   * @return The aspect bean.
   */
  @Bean
  ObservedAspect observedAspect(ObservationRegistry observationRegistry) {
    return new ObservedAspect(observationRegistry);
  }
}