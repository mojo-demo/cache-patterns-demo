package com.mojosoft.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.mojosoft.demo.cache.MatchKey;
import com.mojosoft.demo.dto.FootballResultDTO;
import com.mojosoft.demo.entity.FootballResult;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaDelete;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Answers;

@ExtendWith(MockitoExtension.class)
class FootballResultDbServiceTest {

  @Mock
  private EntityManager entityManager;

  @InjectMocks
  private FootballResultDbService footballResultDbService;


  @Test
  @DisplayName("Delete all football results by match keys")
  public void testDeleteAllByMatchKeys() {
    FootballResultDTO sampleFootballResult1 = getSampleFootballResult();
    FootballResultDTO sampleFootballResult2 = getSampleFootballResult2();
    MatchKey key1 = MatchKey.createMatchKey(sampleFootballResult1);
    MatchKey key2 = MatchKey.createMatchKey(sampleFootballResult2);

    List<MatchKey> keys = List.of(key1, key2);

    CriteriaBuilder cb = mock(CriteriaBuilder.class);
    CriteriaDelete<FootballResult> criteriaDelete = mock(CriteriaDelete.class, Answers.RETURNS_DEEP_STUBS);
    Root<FootballResult> root = mock(Root.class, Answers.RETURNS_DEEP_STUBS);
    TypedQuery<Integer> query = mock(TypedQuery.class, Answers.RETURNS_DEEP_STUBS);

    Predicate predicate = mock(Predicate.class);

    when(entityManager.getCriteriaBuilder()).thenReturn(cb);
    when(cb.createCriteriaDelete(FootballResult.class)).thenReturn(criteriaDelete);
    when(criteriaDelete.from(FootballResult.class)).thenReturn(root);
    when(cb.and(any(), any(), any())).thenReturn(predicate);
    when(entityManager.createQuery(criteriaDelete)).thenReturn(query);

    footballResultDbService.deleteAllByMatchKeys(keys);

    ArgumentCaptor<Predicate[]> predicatesCaptor = ArgumentCaptor.forClass(Predicate[].class);
    verify(cb).or(predicatesCaptor.capture());
    Predicate[] capturedPredicates = predicatesCaptor.getValue();

    assertThat(capturedPredicates).hasSize(keys.size());
    verify(query).executeUpdate();
  }

  private FootballResultDTO getSampleFootballResult() {
    return new FootballResultDTO("21/22", "Stadium1", "HomeTeam1", "AwayTeam1", 2, 1, LocalDateTime.now());
  }

  private FootballResultDTO getSampleFootballResult2() {
    return new FootballResultDTO("21/22", "Stadium2", "HomeTeam2", "AwayTeam2", 1, 1, LocalDateTime.now());
  }
}