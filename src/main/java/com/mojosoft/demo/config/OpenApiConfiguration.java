package com.mojosoft.demo.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;
import java.util.Collections;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * This class is used to set up the OpenAPI configuration.
 */
@Setter
@Configuration
public class OpenApiConfiguration {

  @Value("${server.port}")
  private String serverPort;

  @Value("${server.servlet.context-path}")
  private String path;

  /**
   * This method is used to set up the OpenAPI configuration.
   *
   * @return The OpenAPI configuration
   */
  @Bean
  public OpenAPI participantsOpenApi() {
    return new OpenAPI()
        .info(new Info().title("Cache Pattern Demo API")
            .description("An API to demonstrate "
                + "'read/write through' and 'write-behind' caching using Hazelcast in Spring Boot")
            .version("v0.0.1")
            .license(new License().name("Apache 2.0").url("http://springdoc.org"))
            .contact(new Contact()
                .name("Jon")
                .email("jon@somewhere.com")
                .url("https://www.jon.somwhere.com/"))
        )
        .servers(Collections.singletonList(
                new Server().url("http://localhost:" + serverPort + path).description("Local server")
        ));
  }
}