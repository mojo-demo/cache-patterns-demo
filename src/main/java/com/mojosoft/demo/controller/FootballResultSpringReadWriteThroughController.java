package com.mojosoft.demo.controller;

import com.mojosoft.demo.controller.base.FootballResultControllerBase;
import com.mojosoft.demo.service.FootballResultSpringReadWriteThroughService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class for testing football results with read/write through cache.
 */
@RestController
@Tag(name = "Football Results (Spring Cache Read/Write Through)")
@RequestMapping("/api/v1/football-results/spring-readwrite-through")
@Slf4j
public class FootballResultSpringReadWriteThroughController extends FootballResultControllerBase {

  public FootballResultSpringReadWriteThroughController(
      FootballResultSpringReadWriteThroughService resultService) {
    super(resultService);
  }
}
