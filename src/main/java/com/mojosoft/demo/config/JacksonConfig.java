package com.mojosoft.demo.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * JaksonConfig.
 */
@Configuration(proxyBeanMethods = false)
public class JacksonConfig {

  /**
   * ObjectMapper with JavaTimeModule.
   *
   * @return ObjectMapper with JavaTimeModule
   */
  @Bean
  public ObjectMapper objectMapper() {
    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new JavaTimeModule());
    return mapper;
  }
}