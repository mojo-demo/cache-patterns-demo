package com.mojosoft.demo.utils;

public class StringUtils {
  public static String wrapInDoubleQuotes(String input) {
    return "\"" + input + "\"";
  }
}