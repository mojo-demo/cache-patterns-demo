package com.mojosoft.demo.service;

import com.mojosoft.demo.mapper.MapperHelper;
import com.mojosoft.demo.repository.FootballResultRepository;
import com.mojosoft.demo.repository.FootballResultSaveOrUpdateHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class FootballResultSpringReadWriteThroughServiceTest {

  @Mock
  private FootballResultRepository footballResultRepository;

  @Mock
  private FootballResultSaveOrUpdateHelper footballResultSaveOrUpdateHelper;

  @Mock
  private MapperHelper mapper;

  @InjectMocks
  private FootballResultSpringReadWriteThroughService footballResultService;

  private FootballResultServiceTestHelper helper;

  @BeforeEach
  void setup() {
    helper = new FootballResultServiceTestHelper(
        footballResultService,
        footballResultRepository,
        footballResultSaveOrUpdateHelper,
        mapper);
  }

  @Test
  @DisplayName("Save football results without cache")
  void testSaveResults() {
    helper.assertSaveResults();
  }

  @Test
  @DisplayName("Retrieve all football results")
  void testGetAll() {
    helper.assertGetAll();
  }

  @Test
  @DisplayName("Retrieve football results by season")
  void testGetBySeason() {
    helper.assertGetBySeason();
  }

  @Test
  @DisplayName("Retrieve football result by season, home team, and away team")
  void testGetByMatch() {
    helper.assertGetByMatch();
  }
}